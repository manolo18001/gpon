<nav id="nav-xs-ver" class="col-xs-2 navbar navbar-expand-lg navbar-s1 navbar-side" role="navigation">

    <div style="height: auto;" class="col-xs-12">

        <ul class="nav side-nav" id="side">
            <br>

            <!-- <li label="Inicio" icon-left="fa fa-3x fa-home" aria-toggle="fa fa-lg fa-" aria-collapsed="fa fa-lg fa-" id-children="Inicio251369" id-parent="side" class="panel text panel nav collapse first">

                <label for="">&nbsp; Aprovisionador</label>

            </li>


            <li label="Opciones" class="panel" id-parent="side">
                <a href="{{url('/central')}}" class="sublink">
                    <span class="text">&nbsp;&nbsp; Central </span>
                </a>
            </li>

            <li label="Opciones" class="panel" id-parent="side">

                <a href="{{url('/aprovicion')}}" class="sublink">
                    <span class="text">&nbsp;&nbsp; OLT </span>
                </a>


            </li> -->
            @can('users.index')
            <li label="Servicios Telefónicos" icon-left="fa fa-lg fa-th" aria-toggle="fa fa-lg fa-chevron-down" aria-collapsed="fa fa-lg fa-chevron-left" class="panel first" id-children="aprovisionador0101" id-parent="Aprosionador">
                <div aria-expanded="false" data-parent="#Aprosionador" data-toggle="collapse" data-target="#aprovisionador0101" class="accordion-toggle collapsed">
                    <span class="text">
                        <!-- <i class="fa fa-lg fa-wrench pull-left"></i> -->
                        Aprovisionador
                    </span>
                    <span class="fa fa-lg fa-chevron-down aria-toggle" aria-hidden="true"></span>
                    <span class="fa fa-lg fa-chevron-left aria-collapsed" aria-hidden="true"></span>
                </div>
                <ul id="aprovisionador0101" class="nav collapse menu_level_2">
                    <!-- {{-- <li label="central" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388720" id-parent="aprovisionador0101">
                        <a href="{{url('/central')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Central
                        </a>
                    </li> --}} -->
                    <li label="aprovision" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="aprovisionador0101">
                        <a href="{{url('/aprovision')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            OLT
                        </a>
                    </li>
                    <li label="tarjetas y puertos" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="aprovisionador0101">
                        <a href="{{url('/TargetPort')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Tarjetas y puertos
                        </a>
                    </li>
                    <!-- <li label="ONT" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="aprovisionador0101">
                        <a href="{{url('/ont')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            ONT
                        </a>
                    </li> -->
                </ul>
            </li>
            @endcan
            @can('roles.index')
            <li label="Servicios Telefónicos" icon-left="fa fa-lg fa-th" aria-toggle="fa fa-lg fa-chevron-down" aria-collapsed="fa fa-lg fa-chevron-left" class="panel first" id-children="planes_productos" id-parent="Planes_productos">
                <div aria-expanded="false" data-parent="#Planes_productos" data-toggle="collapse" data-target="#planes_productos" class="accordion-toggle collapsed">
                    <span class="text">
                        <!-- <i class="fa fa-lg fa-wrench pull-left"></i> -->
                        Instalación 
                    </span>
                    <span class="fa fa-lg fa-chevron-down aria-toggle" aria-hidden="true"></span>
                    <span class="fa fa-lg fa-chevron-left aria-collapsed" aria-hidden="true"></span>
                </div>
                <ul id="planes_productos" class="nav collapse menu_level_2">
                    <!-- <li label="cambiodeplantelefonico" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388720" id-parent="planes_productos">
                        <a href="{{url('/service')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Perfil de servicio
                        </a>
                    </li>-->
                    <li label="servicios complementarios telefonicos" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="planes_productos">
                        <a href="{{route('Odf.index')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            ODF
                        </a>
                    </li>
                    <li label="servicios complementarios telefonicos" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="planes_productos">
                        <a href="{{route('manga.index')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Manga
                        </a>
                    </li> 
                    <li label="servicios complementarios telefonicos" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="planes_productos">
                        <a href="{{route('splitter.index')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Splitter
                        </a>
                    </li> 
                    <li label="servicios complementarios telefonicos" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="planes_productos">
                        <a href="{{route('installggpm.index')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Instalacion GGPM
                        </a>
                    </li>
                   <li label="servicios complementarios telefonicos" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="planes_productos">
                        <a href="{{route('ggto.index')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Instalacion GGTO
                        </a>
                    </li>



                </ul>
            </li>
            @endcan

            {{-- <li label="Servicios Telefónicos" icon-left="fa fa-lg fa-th" aria-toggle="fa fa-lg fa-chevron-down" aria-collapsed="fa fa-lg fa-chevron-left" class="panel first" id-children="Mantenimiento001" id-parent="Mantenimiento">
                <div aria-expanded="false" data-parent="#Mantenimiento" data-toggle="collapse" data-target="#Mantenimiento001" class="accordion-toggle collapsed">
                    <span class="text">
                        <!-- <i class="fa fa-lg fa-wrench pull-left"></i> -->
                        Gestión y Mantenimiento
                    </span>
                    <span class="fa fa-lg fa-chevron-down aria-toggle" aria-hidden="true"></span>
                    <span class="fa fa-lg fa-chevron-left aria-collapsed" aria-hidden="true"></span>
                </div>
                <ul id="Mantenimiento001" class="nav collapse menu_level_2">
                    {{-- <li label="sfp" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388720" id-parent="Mantenimiento001">
                        <a href="{{url('/sfp')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            SFP
                        </a>
                    </li> --}}
                    <!-- <li label="tipo" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="Mantenimiento001">
                        <a href="{{url('/tipo')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Tipo ONT
                        </a>
                    </li> -->
                    {{-- <li label="tarjetas y puertos" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="Mantenimiento001">
                        <a href="{{url('/proveedor')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Proveedor
                        </a>
                    </li> --}}
                    <!-- <li label="Opeinternas" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="Mantenimiento001">
                        <a href="{{url('/Opeinternas')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Operaciones internas
                        </a>
                    </li>
                    <li label="Gciaoperaciones" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="Mantenimiento001">
                        <a href="{{url('/operaciones')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Gerencia de operaciones
                        </a>
                    </li> -->
                {{-- </ul>
            </li> --}}

            @can('central.index')
            <!-- <li label="Opciones" class="panel" id-parent="side">
                <div>
                    <a href="{{url('/install')}}" class="sublink inactive">
                        <span class="text">&nbsp;&nbsp;Instalación</span>
                    </a>
                </div>
            </li>
            <li label="Opciones" class="panel" id-parent="side">
                <div>
                    <a href="{{url('/ordenes')}}" class="sublink inactive">
                        <span class="text">&nbsp;&nbsp;Ordenes</span>
                    </a>
                </div>
            </li> -->
            @endcan
            @can('config.index')

            <li label="Servicios Telefónicos" icon-left="fa fa-lg fa-th" aria-toggle="fa fa-lg fa-chevron-down" aria-collapsed="fa fa-lg fa-chevron-left" class="panel first" id-children="configuracion" id-parent="configuracion">
                <div aria-expanded="false" data-parent="#Configuracion" data-toggle="collapse" data-target="#configuracion" class="accordion-toggle collapsed">
                    <span class="text">
                        <!-- <i class="fa fa-lg fa-wrench pull-left"></i> -->
                         Configuración
                    </span>
                    <span class="fa fa-lg fa-chevron-down aria-toggle" aria-hidden="true"></span>
                    <span class="fa fa-lg fa-chevron-left aria-collapsed" aria-hidden="true"></span>
                </div>
                <ul id="configuracion" class="nav collapse menu_level_2">
                    <li label="central" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="configuracion">
                        <a href="{{url('/central')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Central
                        </a>
                     </li>    
                        <!-- <li label="Opeinternas" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="configuracion">
                            <a href="{{url('/install')}}" class="sublink">
                                <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                                Instalación
                            </a>
                        </li> -->
                    <li label="Gciaoperaciones" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="configuracion">
                        <a href="{{url('/ordenes')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Órdenes
                        </a>
                    </li>
                    <!-- agregado -->
                    <li label="cambiodeplantelefonico" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388720" id-parent="configuracion">
                        <a href="{{url('/service')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Perfil de Servicio
                        </a>
                    </li>
                    <li label="servicios complementarios telefonicos" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="configuracion">
                        <a href="{{url('/online')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Perfil de Línea
                        </a>
                    </li>
		            <li label="servicios complementarios telefonicos" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="configuracion">
                        <a href="{{url('/config_card')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Tarjeta
                        </a>
                    </li>

                    
                    <li label="sfp" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388720" id-parent="configuracion">
                        <a href="{{url('/sfp')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            SFP
                        </a>
                    </li>

                    <li label="tarjetas y puertos" icon-left="fa fa-lg fa-angle-double-right" class="first" id-children="Actualizar_Datos388721" id-parent="configuracion">
                        <a href="{{url('/proveedor')}}" class="sublink">
                            <i class="fa fa-lg fa-angle-double-right fa-lg pull-left"></i>
                            Proveedor
                        </a>
                    </li>
                    
		    


                    <!-- agregado -->
                <!-- <li label="Opciones" class="panel" id-parent="side">
                <div>
                    <a href="{{url('/install')}}" class="sublink">
                        <span class="text">&nbsp;&nbsp;Instalación</span>
                    </a>
                </div>
            </li>
            <li label="Opciones" class="panel" id-parent="side">
                <div>
                    <a href="{{url('/ordenes')}}" class="sublink">
                        <span class="text">&nbsp;&nbsp;Ordenes</span>
                    </a>
                </div>
            </li> -->
            @endcan


        </ul>
</li>

    </div>

</nav>
