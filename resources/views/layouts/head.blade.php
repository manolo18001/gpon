<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="DC.format" content="text/plain">
    <meta name="DC.language" content="es">
    <meta name="DC.creator" content="Luis Herazo">
    <meta name="DC.date.modified" content="2017">
    <meta name="DC.date.created" content="2017">
    <meta name="DC.type" content="">
    <meta name="DC.distribution" content="Global">
    <meta name="keywords" content="Autogestion">
    <meta name="robots" content="ALL">
    <meta name="distribution" content="Global">
    <meta name="viewport" content="width=device-width, initial-scale=0.6666, maximum-scale=1.0, minimum-scale=0.6666">
    <meta name="generator" content="">

    <title>CANTV | GPON</title>

    <!--Inicialización de CSS básico-->
    <link href='{{asset("static/img/favicon.ico")}}' rel="shortcut icon" />
    <!-- Extras -->
    <link rel="stylesheet" type="text/css" href="{{ asset('static/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('static//css/jquery-ui.css')}}">



    <link rel="stylesheet" type="text/css" href="{{ asset('static/css/cantv.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('static/css/custom.css')}}">

    <!-- <link rel="stylesheet" type="text/css" href="static/css/layout.css"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('static/css/base.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('static/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('static/css/menus.css')}}">

    <!-- DATATABLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('static/datatables/jquery.dataTables.min.css')}}">
    <link type="text/css" href="{{ asset('static/datatables/buttons.dataTables.min.css')}}" rel="stylesheet" />
    <link type="text/css" href="{{ asset('static/datatables/responsive.dataTables.min.css')}}" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="{{ asset('static/datatables/css/bootstrap-select.min.css')}}">

    <script type="text/javascript" src="{{ asset('static/js/jquery/jquery-3.1.1.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('static/js/jquery/jquery-ui.js')}}"></script>


    <script type="text/javascript" src="{{asset('static/bootstrap/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('static/js/hora.js')}}"></script>
    <script type="text/javascript" src="{{asset('static/js/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('static/js/validacion.js')}}"></script>
    <script type="text/javascript" src="{{asset('static/js/valido.js')}}"></script>
    <!-- DATATABLES -->
    <script type="text/javascript" src="{{ asset('static/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('static/datatables/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('static/datatables/buttons.html5.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('static/datatables/buttons.colVis.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('static/datatables/jszip.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('static/datatables/pdfmake.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('static/datatables/vfs_fonts.js')}}"></script>

    <script type="text/javascript" src="{{ asset('static/datatables/js/bootbox.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('static/datatables/js/bootstrap-select.min.js')}}"></script>
</head>
