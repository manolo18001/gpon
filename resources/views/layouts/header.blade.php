		<!-- .main-header / Encabezado principal -->
		<div class="main-container container">
		    <header class="main-header">
		        <!-- barra gbv -->
		        <div class="barra-gbv">
		            <div class="logo-gbv">
		                <img src="{{ asset('static/img/mincyt.png')}}" alt="Gobierno Bolivariano de Venezuela" />
						
		            </div>
					<div class="logo-gbv2">
						<img src="{{ asset('static/img/logo_cantv_c.png')}}" alt="Gobierno Bolivariano de Venezuela" />
					</div>

		            <!-- <div class="logo-id">
		                <img src="https://www2.cantv.com.ve/cantv_com_ve/++theme++cantv.theme/@@cantv.headerimages/image_2" alt="Juventud Bicentenaria" />
		            </div> -->
		        </div>
		        <!--// barra gbv -->

		        <!-- header top -->
		        <div class="header-top" style="position: relative;">


		            <div class="user-account">

		            </div>




		        </div>
		        <!--// header top -->

		        <!-- navbar -->
		        <nav class="navbar navbar-default" role="navigation">
		            <!-- Brand and toggle get grouped for better mobile display -->
		            <div class="navbar-header">
		                <!-- -->
		                <!--<button type="button" class="navbar-toggle toggle-menu">
							<span class="sr-only">Abrir menú</span>
						</button>-->
		                <!-- -- >
						<button type="button" class="navbar-toggle toggle-search" data-toggle="collapse" data-target="#search-bar">
							<span class="sr-only">Abrir búsqueda</span>
						</button>
						<!-- -->

		                <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2 pull-left">
		                    <a class="navbar-brand" id="logo-cantv" href="http://www2.cantv.com.ve">
		                        <img src="{{asset('static/img/logo_cantv_c.png')}}" class="logo-cantv" alt="Cantv.com.ve" />
		                    </a>
		                </div>

		                <div class="hidden-xs col-sm-6 col-md-6 col-lg-6 title-center">
		                    <h3 class="header-title">Aprovisionador GPON</h3>
		                </div>
		                <div class="hidden-xs hidden-sm col-md-4 col-lg-4 pull-right fecha-hora">
		                    <div align="right" id="bienvenido">

		                        @guest
		                        @if (Route::has('register'))
		                        @endif
		                        @else
		                        <span class="bienvenido">Bienvenido: &nbsp;</span>
		                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
		                            {{ Auth::user()->name }}
		                        </a>
		                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
		                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
		                                {{ __('Cerrar sesión') }}
		                            </a>
		                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
		                                @csrf
		                            </form>
		                        </div>
		                        @endguest
		                        <br>

		                        <div align="right">
		                            <div class="reloj" id="reloj"></div>
		                        </div>
		                        <!-- <div class="reloj" id="reloj"></div> -->
		                    </div>

		                </div>

		        </nav>
		        <!--// navbar -->
		    </header>
		</div>
		<!--// .main-header -->
		<main class="container">
