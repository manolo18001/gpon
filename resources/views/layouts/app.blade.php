@include('layouts.head')
@include('layouts.header')



<body>

    <div class="container">

        @yield('content')

    </div>

</body>
@include('layouts.footer')
@yield('script')

</html>
