</main>
    <!-- Pie de p&#225;gina -->
		<footer class="main-footer" id="main-footer">
		    <div id="portal-footer">
		        <div class="container">
		            <!-- .footer-menus / Bloque de menús inferiores -->
		            <div class="footer-menus row"></div>
		            <!--// .footer-menus -->

		            <!-- .footer-logos / Barra con logos -->
		            <div class="footer-logos">
		                <a href="http://www.cantv.com.ve/">
		                    <img src="{{ asset('static/img/logo_cantv_c.png')}}" alt="Cantv ">
		                </a>
		                <!--<a href="http://www.cantv.net/">
							<img src="../img/logo_cantv_net_c.png" alt="Cantv.net">
						</a>
						<a href="http://www.pac.com.ve/">
							<img src="../img/logo_paginas_amarillas_copia.png" alt="Páginas Amarillas">
						</a>-->
		            </div>
		            <!--// .footer-logos -->

		            <!-- .footer-info / Barra inferior con información final -->
		            <div class="footer-info">
		                <!-- copyright -->
		                <div class="footer-info-left">
		                    <strong>Cantv.com.ve</strong> | Rif: J-00124134-5 | Todos los derechos reservados.
		                </div>
		                <!--// copyright -->
		                <!-- termino y condiciones -->
		                <div class="footer-info-right">
		                    <a href="https://www2.cantv.com.ve/cantv_com_ve/terminos-y-condiciones">Términos y condiciones</a>
		                    <!--<span>|</span>
							<a href="mapadelsitio.php">Mapa de sitio</a>
							<span>|</span>
							<a href="#">Anúnciate con nosotros</a>-->
		                </div>
		                <!--// termino y condiciones -->
		            </div>
		            <!--// .footer-info -->
		        </div>

		        <!-- Barra flotante con enlaces a redes sociales -->
		        {{-- <div class="redes-sociales-float">
		            <ul>

		                <li>
		                    <a href="https://www.facebook.com/ConexionCulturalCantv" title="" target="_blank">
		                        <img src="{{asset('static/img/logo_facebook.png')}}">
		                    </a>
		                </li>
		                <li>
		                    <a href="https://www.youtube.com/user/Cantvsalaprensa" title="" target="_blank">
		                        <img src="{{asset('static/img/logo_youtube.png')}}">
		                    </a>
		                </li>
		                <li>
		                    <a href="https://twitter.com/salaprensaCantv" title="" target="_blank">
		                        <img src="{{asset('static/img/logo_twitter.png')}}">
		                    </a>
		                </li>
		                <li>
		                    <a href="https://www.instagram.com/contactocantv/" title="" target="_blank">
		                        <img src="{{asset('static/img/logo_instagram.png')}}">
		                    </a>
		                </li>
		            </ul>
		        </div> --}}
		        <!--// Barra flotante con enlaces a redes sociales -->
		    </div>
		</footer>
		<!--// Pie de p&#225;gina -->
