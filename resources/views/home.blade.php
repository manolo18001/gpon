@extends('layouts.app')

@section('content')
@inject('estados', 'App\Services\estados')

<body class="container">

    <div class="main-container container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
                <!-- <form name="form" method="post" class="form-horizontal" action="{{url('/central')}}" enctype="multipart/form-data"> -->
                {{ csrf_field() }}


                <div class="aside">
                    @include('layouts.menu_left')

                </div>


                <div class='col-xs-8 col-sm-8 col-md-8 col-xl-8 col-xs-offset-1 col-md-offset-1 col-sm-offset-1'>

                    <div class='class="col-xs-12"'>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                                <!-- <li class="breadcrumb-item active" aria-current="page">Central</li> -->
                            </ol>
                        </nav>
                    </div>
                    <div class='col-xs-12 col-sm-12 col-md-12 col-xl-12'>
                        <h3 class="gris"> <strong>Bienvenido(a) al aprovisionador GPON</strong> </h3>
                        <!-- <hr /> -->
                        <!-- <p class="label-inf">Completa todos los campos del formulario con los datos solicitados.</p> -->
                        <!-- <hr /> -->

                    </div>

                </div>
                <!-- <div class='col-xs-12 col-sm-12 col-md-12 col-xl-12'>
                            <hr />
                        </div> -->


                <!-- </form> -->
            </div>
        </div>
    </div>
</body>
@endsection
