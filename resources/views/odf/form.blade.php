@extends('layouts.app')

@section('content')

<body class="container">

    <div class="main-container container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
                {{ csrf_field() }}
                <!-- datos personales-->
                <div class='col-xs-12 col-sm-12 col-md-12 col-xl-12'>
                    <!-- <span class="badge pull-right numero_formulario_registro">1</span> -->
                    <h3 class="gris"><strong> ODF </strong></h3>
                    <hr />
                    <!-- <p class="label-inf">Completa todos los campos del formulario con los datos solicitados.</p> -->
                    <!-- <hr /> -->
                </div>
                <div class="aside">
                    @include('layouts.menu_left')
                </div>
                <div class='col-xs-8 col-sm-8 col-md-8 col-xl-8 col-xs-offset-1 col-md-offset-1 col-sm-offset-1'>
                    <div class="panel-body table-responsive" id="listadoregistros">
                             <div class='class="col-xs-12"'>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Aprovisionamiento</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">ODF</li>
                                    </ol>
                                </nav>
                            </div>
                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                            <thead>
                                <th>#</th>
                                <th>ODF</th>
                                <th>Sala</th>
                                <th>piso</th>
                                <th>Opciones</th>
                            </thead>
                            <tbody>
                                @foreach($odfs as $odf)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ $odf->odf}}</td>
                                    <td>{{ $odf->sala}}</td>
                                    <td>{{ $odf->piso}}</td>
                                    
                                    <td>
                                        <a href="{{ route('Odf.edit',$odf->id)}}">
                                            <button class="btn btn-warning" onclick="">Editar<i class="fa fa-pencil"></i></button>
                                        </a>
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
</body>
@endsection
