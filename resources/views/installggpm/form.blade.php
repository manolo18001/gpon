@extends('layouts.app')

@section('content')

<body class="container">

    <div class="main-container container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">

                {{ csrf_field() }}


                <!-- datos personales-->

                <div class='col-xs-12 col-sm-12 col-md-12 col-xl-12'>
                    <!-- <span class="badge pull-right numero_formulario_registro">1</span> -->
                    <h3 class="gris"><strong> Instalación GGPM </strong></h3>
                    <hr />
                    <!-- <p class="label-inf">Completa todos los campos del formulario con los datos solicitados.</p> -->
                    <!-- <hr /> -->

                </div>


                <div class="aside">
                    @include('layouts.menu_left')

                </div>


                <div class='col-xs-8 col-sm-8 col-md-8 col-xl-8 col-xs-offset-1 col-md-offset-1 col-sm-offset-1'>

                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                            <thead>
                                <th>#</th>
                                <!-- <th>Ip del agregador</th> -->
                                <th>Central </th>
                                <th>OLT </th>
                                <th>Tarjeta </th>
                                <th>ODF </th>
                                <th>Opciones</th>
                            </thead>
                            <tbody>


                            </tbody>
                            <!-- <tfoot> -->
                            <!-- <th>#</th> -->
                            <!-- <th>Ip del agregador</th> -->
                            <!-- <th>Código de la central</th> -->
                            <!-- <th>Nombre de la central</th> -->
                            <!-- <th>Opciones</th> -->
                            <!-- </tfoot> -->
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
</body>
@endsection
