@extends('layouts.app')

@section('content')

<body class="container">

    <div class="main-container container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">

                {{ csrf_field() }}


                <!-- datos personales-->

                <div class='col-xs-12 col-sm-12 col-md-12 col-xl-12'>
                    <!-- <span class="badge pull-right numero_formulario_registro">1</span> -->
                    <h3 class="gris"><strong> Instalación GGTO </strong></h3>
                    <hr />
                    <!-- <p class="label-inf">Completa todos los campos del formulario con los datos solicitados.</p> -->
                    <!-- <hr /> -->

                </div>


                <div class="aside">
                    @include('layouts.menu_left')

                </div>


                <div class='col-xs-8 col-sm-8 col-md-8 col-xl-8 col-xs-offset-1 col-md-offset-1 col-sm-offset-1'>

                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                            <thead>
                                <th>#</th>
                                <th>ruta</th>
                                <th>Drop</th>
                                <th>Elemento Saliente </th>
                                <th>Opciones</th>
                            </thead>
                            <tbody>

                                @foreach($ggtos as $ggto)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$ggto->ruta}}</td>
                                        <td>{{$ggto->Drop}}</td>
                                        <td>{{$ggto->elementoSaliente}}</td>
                                        <td>
                                            <a href="{{ url('ggto/'.$ggto->id.'/edit')}}">
                                                <button class="btn btn-warning" onclick="">Editar<i class="fa fa-pencil"></i></button>
                                            </a>
                                        </td>

                                    </tr>
                                @endforeach

                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
</body>
@endsection
