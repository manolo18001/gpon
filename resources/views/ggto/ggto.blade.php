@extends('layouts.app')

@section('content')
@inject('estados', 'App\Services\estados')

<body class="container">

    <div class="main-container container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
                <form id="centalForm" name="form" method="post" class="form-horizontal" action="{{url('/ggto')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div id="formulario_registro">

                        <!-- datos personales-->

                        <div class='col-xs-12 col-sm-12 col-md-12 col-xl-12'>
                            <h3 class="gris"><strong> GGTO </strong></h3>
                            <hr />
                            <p class="label-inf">Completa todos los campos del formulario con los datos solicitados.</p>
                            <hr />

                        </div>


                        <div class="aside">
                            @include('layouts.menu_left')

                        </div>


                        <div class='col-xs-8 col-sm-8 col-md-8 col-xl-8 col-xs-offset-1 col-md-offset-1 col-sm-offset-1'>

                            <div class='class="col-xs-12"'>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Instalacion</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">GGTO</li>
                                    </ol>
                                </nav>
                            </div>
                            <div class="form-group col-xs-12">
                                <label class="control-label col-xs-6 required" >Splitter<label class="rojo">*</label></label>
                                <div class=''>
                                    <div class='col-xs-6'>
                                        <select name="id_splitter" id="id_splitter" class="form-control required">
                                            <option value="">Splitter</option>
                                            @foreach($splitter as $split)
                                                <option value="{{$split->id}}">{{$split->splitter}}</option>
                                            @endforeach
                                        </select>
                                        {!! $errors->first('id_splitter','<span class="alert-danger fade in">:message</span>')!!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-xs-12">
                                <label class="control-label col-xs-6 required">Drop<label class="rojo">*</label></label>
                                <div class=''>
                                    <div class='col-xs-6'>
                                        <input type="text" id="drop" required="required" name="drop" placeholder="Drop" class="form-control tip" maxlength="25" title="Ingresa el número sin puntos" />
                                        {!! $errors->first('drop','<span class="alert-danger fade in">:message</span>')!!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12">
                                <label class="control-label col-xs-6 required" for="form_name_central">Distancia</label>
                                <div class=''>
                                    <div class='col-xs-6'>
                                        <input type="text" id="distancia"  name="distancia" placeholder="Distancia" class="form-control tip" maxlength="25"  title="Ingresa el número sin puntos" />
                                        {!! $errors->first('distancia','<span class="alert-danger fade in">:message</span>')!!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12">
                                <label class="control-label col-xs-6 required" for="form_producto_1">Ruta<label class="rojo">*</label></label>
                                <div class=''>
                                    <div class='col-xs-6'>
                                        <!-- <input type="checkbox" id="producto_1" name="producto_1" required="required" placeholder="Fibra Empresarial" class="form-control tip" maxlength="9" title="Ingresa el número sin puntos" /> -->
                                        <input type="text" id="ruta" required="required" name="ruta" placeholder="Ruta" class="form-control required" />
                                        {!! $errors->first('ruta','<span class="alert-danger fade in">:message</span>')!!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-xs-12">
                                <label class="control-label col-xs-6 required" for="form_direcc">Elemento saliente<label class="rojo">*</label></label>
                                <div class=''>
                                    <div class="col-xs-6">
                                        <select name="elementoSaliente" id="elementoSaliente" class="form-control required">
                                            <option value="">Elemento saliente</option>
                                            <option value="Fbx">Fbx</option>
                                            <option value="Terminal">Terminal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class='col-xs-12 col-sm-12 col-md-12 col-xl-12'>
                            <hr />
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-xl-4  form-group">
                            <p>(*)Campo obligatorio.</p>
                            <div class="form-group  pull-right">
                                <button type="submit" class="btn btn-primary" onClick="">Crear</button>
                                <a href="{{ url('/ggtos') }}">
                                    <button type="button" class="btn btn-primary" onClick="">Mostrar Todos</button></a>

                            </div>
                        </div>
                    </div>
                    <!-- fin de la primera parte del formulario-->
                </form>
            </div>
        </div>
    </div>
</body>
<style>
    .centro{
        margin-left: -15px;
    }
</style>
@endsection
