<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('ApiOrden',ApiOrdenController::class)->except('create','edit');

Route::apiResource('ApiPlan',ApiPlanController::class)->except('create','edit');

//acceder a 10.53.248.146/api/ApiPlan para ver todos los registros de los planes

Route::apiResource('ApiProductos',ApiProductController::class)->except('create','edit');

Route::apiResource('ApiDisponibilidad',ApiDisponibilidadController::class);