<?php

namespace App\Http\Controllers;

use App\relacion_flujo;
use Illuminate\Http\Request;

class RelacionFlujoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\relacion_flujo  $relacion_flujo
     * @return \Illuminate\Http\Response
     */
    public function show(relacion_flujo $relacion_flujo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\relacion_flujo  $relacion_flujo
     * @return \Illuminate\Http\Response
     */
    public function edit(relacion_flujo $relacion_flujo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\relacion_flujo  $relacion_flujo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, relacion_flujo $relacion_flujo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\relacion_flujo  $relacion_flujo
     * @return \Illuminate\Http\Response
     */
    public function destroy(relacion_flujo $relacion_flujo)
    {
        //
    }
}
