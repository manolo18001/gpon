<?php

namespace App\Http\Controllers;

use App\saleEquipo;
use Illuminate\Http\Request;

class SaleEquipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\saleEquipo  $saleEquipo
     * @return \Illuminate\Http\Response
     */
    public function show(saleEquipo $saleEquipo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\saleEquipo  $saleEquipo
     * @return \Illuminate\Http\Response
     */
    public function edit(saleEquipo $saleEquipo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\saleEquipo  $saleEquipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, saleEquipo $saleEquipo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\saleEquipo  $saleEquipo
     * @return \Illuminate\Http\Response
     */
    public function destroy(saleEquipo $saleEquipo)
    {
        //
    }
}
