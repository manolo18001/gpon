<?php

namespace App\Http\Controllers;

use App\central;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiDisponibilidadController extends Controller
{
    
    
    public function store(Request $request)
    {
        
        
        //funciones
        function noDisponible()
        {
            return "no hay diponibilidad";
        }


        //datos de direccion cliente
        $longitud = $request->logitud;
        $latitud = $request->latitud;

        //buscar tao 
        $taos=DB::table('taos')
            ->select('id','latitud','longitud')
        ->get();

        
        
        /*//obtener central
        $centrals=DB::table('centrals')
            ->select('centrals.id','name_central')
            ->join('estados','centrals.estado_id','=','estados.id')
            ->join('ciudads','centrals.ciudad_id','=','ciudads.id')
            ->join('municipios','centrals.municipio_id','=','municipios.id')
            ->join('parroquias','centrals.parroquia_id','=','parroquias.id')
            ->where([
                   
                ['estados.estado','=',"$estado"],
                ['ciudads.ciudad','=',"$ciudad"],
                ['municipios.municipio','=',"$municipio"],
                ['parroquias.parroquia','=',"$parroquia"],
    
            ])
        ->get();
        
        $central_name=array();
        $id_central=array();
        foreach($centrals as $indice=>$central)
        {
            array_push($id_central,$central->id);
            array_push($central_name,$central->name_central);
        }
        
        //comprobacion de existencia de la central

        if(!empty($id_central))
        { 
        
            //obtener olt
            $olts=DB::table('olts')
            ->select('id',)
            ->where('central_id',$id_central)
            ->get();
            
            if(!empty($olts))
            {
                //obtener tarjetas
                $targets=array();
                foreach($olts as $indice=>$o)
                {
                    $target_ports=DB::table('target_ports')
                    ->where('olt_id','=',$o->id)
                    ->get();
                    
                    foreach($target_ports as $indice=>$target_port)
                    {
                        array_push($targets,$target_port->id);
                    }
                }
                
                if(!empty($targets))
                {
                    //obtener puertos de servicio
                    $sp=array();
                    for($i=0;$i<count($targets);$i++)
                    {
                        
                        
                        $service_ports= DB::table('service_port')
                        ->where([
                            ['id_targetPorts','=',$targets[$i]],
                            ['disponibilidad','=',true],
                        ])
                        ->get();
                        
                        foreach($service_ports as $indice=>$service_port)
                        {
                            array_push($sp,$service_port->id);
                        }

                    }
                    
                    $return="la cantidad de puertos disponibles en la central ". $central_name[0]." es de ".count($sp)." puertos ";

                }
                else
                {
                    $return=noDisponible();
                }
                
            } 
            else
            {
                $return=noDisponible();
            }
            

               
          
        }
        else
        {
            $return=noDisponible().", la central no existe";
        }
        
        return $return;*/

    }

}
