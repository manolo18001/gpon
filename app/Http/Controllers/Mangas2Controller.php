<?php

namespace App\Http\Controllers;

use App\Mangas2;
use Illuminate\Http\Request;

class Mangas2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mangas2  $mangas2
     * @return \Illuminate\Http\Response
     */
    public function show(Mangas2 $mangas2)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mangas2  $mangas2
     * @return \Illuminate\Http\Response
     */
    public function edit(Mangas2 $mangas2)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mangas2  $mangas2
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mangas2 $mangas2)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mangas2  $mangas2
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mangas2 $mangas2)
    {
        //
    }
}
