<?php

namespace App\Http\Controllers;

use Collective\Remote\Connection;
use Illuminate\Http\Request;
require_once '../vendor/laravelcollective/remote/src/Connection.php';

class OltComandsController extends Controller
{
    //
    public function store(Request $request)
    {   
        $host=$request->host;
        $username=$request->username;
        $password=$request->password;
        $frame=$request->frame;
        $slot=$request->slot;
        $puerto=$request->puerto;
        $ont_id=$request->ont_id;
        $serial_modem=$request->serial_modem;
        $id_producto=$request->id_producto;
        $id_plan=$request->id_plan;
        $descripcion_del_cliente=$request->descripcion_del_cliente;
        $puerto_servicio=$request->puerto_servicio;
        $vlan=$request->vlan;
        $inner_vlan=$request->inner_vlan;
        $velocidad_subida=$request->velocidad_subida;
        $velocidad_bajada=$request->velocidad_bajada;
        
        $conexion=[
            'host'      => $host,
            'username'  => $username,
            'password'  => $password,
            'key'       => '',
            'keytext'   => '',
            'keyphrase' => '',
            'agent'     => '',
            'timeout'   => 10,
        ];

        $SSH=new Connection('gpon',$host,$username,$conexion,null,10);
        
        $SSH->run([
            
            'enable',
            'configure',
            "interface gpon $frame/$slot",
            "ont add $puerto $ont_id sn-auth “"."$serial_modem"."” omci ont-lineprofile-id $id_producto ont-srvprofile-id $id_plan desc “".$descripcion_del_cliente."”",
            "ont port native-vlan $puerto $ont_id eth 1 vlan 10 priority 0",
            "service-port $puerto_servicio vlan $vlan gpon 0/$slot/$puerto ont $ont_id gemport 11 multi-service user-vlan 10 tag-transform translate-and-add inner-vlan $inner_vlan inbound traffic-table index $velocidad_subida outbound traffic-table index $velocidad_bajada",
            "mac-address max-mac-count service-port $puerto_servicio 4",
            "commit"
        ]);
        
    }
    

}
