<?php

namespace App\Http\Controllers;

use App\ggto;
use App\Splitter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GgtoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $splitter=DB::table('splitters')
            ->where('nivelDivision','=',2)
        ->get();
        return view('ggto.ggto',['splitter'=>$splitter]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validate = request()->validate(
            [
                'id_splitter'=>'required',
                'drop'=>'required',
                'distancia'=>'required',
                'ruta'=>'required',
                'elementoSaliente'=>'required'
            ]    
        );
        
        try{
            if($validate)
            {
                ggto::insert([
                    'id_splitter'=>$request->id_splitter,
                    'drop'=>$request->drop,
                    'distancia'=>$request->distancia,
                    'ruta'=>$request->ruta,
                    'elementoSaliente'=>$request->elementoSaliente
                ]);
                $respuesta['respuesta'] = array(
                    "title" => "Creacion instalacion de GGTO ",
                    "msg" => "Estimado usuario,instalcion de GGTO se ha creado exitosamente",
                    "ruta" => 'ggto',
                    "otros" => ""
                );
                return view('mensajes.satisfactorio', $respuesta);
            }    
        }catch (\Throwable $th) {
            // throw $th;
            $respuesta['respuesta'] = array(
                "title" => "Creacion instalacion de GGTO ",
                "msg" => "Estimado usuario, no se ha podido realizar la creación de la instalcion de GGTO, intente más tarde.",
                "ruta" => 'ggto',
                "otros" => ""
            );
            return view('mensajes.error', $respuesta);
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ggto  $ggto
     * @return \Illuminate\Http\Response
     */
    public function show(ggto $ggto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ggto  $ggto
     * @return \Illuminate\Http\Response
     */
    public function edit($id_ggto)
    {
        //
        $ggto=ggto::findOrFail($id_ggto);
        $array=json_decode($ggto,true);
        $id_splitter=$array['id_splitter'];
        $splitter=Splitter::findOrFail($id_splitter);
        return view('ggto.edit',['ggto'=>$ggto,'splitter'=>$splitter]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ggto  $ggto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ggto $ggto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ggto  $ggto
     * @return \Illuminate\Http\Response
     */
    public function destroy(ggto $ggto)
    {
        //
    }

    public function all()
    {
        $ggtos=ggto::paginate(100);
        return view('ggto.form',['ggtos'=>$ggtos]);
    }
}
