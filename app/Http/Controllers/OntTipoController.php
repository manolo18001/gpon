<?php

namespace App\Http\Controllers;

use App\ont_tipo;
use Illuminate\Http\Request;

class OntTipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ont_tipo  $ont_tipo
     * @return \Illuminate\Http\Response
     */
    public function show(ont_tipo $ont_tipo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ont_tipo  $ont_tipo
     * @return \Illuminate\Http\Response
     */
    public function edit(ont_tipo $ont_tipo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ont_tipo  $ont_tipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ont_tipo $ont_tipo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ont_tipo  $ont_tipo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ont_tipo $ont_tipo)
    {
        //
    }
}
