<?php

namespace App\Http\Controllers;

use App\puertos;
use Illuminate\Http\Request;

class PuertosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\puertos  $puertos
     * @return \Illuminate\Http\Response
     */
    public function show(puertos $puertos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\puertos  $puertos
     * @return \Illuminate\Http\Response
     */
    public function edit(puertos $puertos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\puertos  $puertos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, puertos $puertos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\puertos  $puertos
     * @return \Illuminate\Http\Response
     */
    public function destroy(puertos $puertos)
    {
        //
    }
}
