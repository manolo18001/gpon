<?php

namespace App\Http\Controllers;

use App\elemento_red;
use Illuminate\Http\Request;

class ElementoRedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\elemento_red  $elemento_red
     * @return \Illuminate\Http\Response
     */
    public function show(elemento_red $elemento_red)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\elemento_red  $elemento_red
     * @return \Illuminate\Http\Response
     */
    public function edit(elemento_red $elemento_red)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\elemento_red  $elemento_red
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, elemento_red $elemento_red)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\elemento_red  $elemento_red
     * @return \Illuminate\Http\Response
     */
    public function destroy(elemento_red $elemento_red)
    {
        //
    }
}
