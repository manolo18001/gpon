<?php

namespace App\Http\Controllers;

use App\target_port;
use App\olt;
use App\status;
use App\puertos;
use App\config_card;
use Illuminate\Http\Request;
// use Illuminate\Http\Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

class TargetPortController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
        $datos['olt'] = olt::paginate(5);
        $datos['status'] = status::all()->except([2,3,4]);
        $datos['config_card'] = config_card::get();
        $datos['puertoini']= 0 ;
        return view('ports.cards_ports', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */
    public function store(Request $request)
    {
        //
        
        $validate = request()->validate(
            //    ['Ipolt'=>'required|ip',
            [
                // 'ip_agregador' => 'required|ip',
                'olt_id' => 'required',
                'ranura_tarjeta'=>'required',
                'id_card'=>'required',
                'VPI'=>'required',
                'S_VLAN'=>'required',
                'puerto_inicial'=>'required',
                'cant_puertos'=> 'required|numeric',
                //'ID_estatus' => 'required',
                'C_lan_ini'=>'required'
            ]
        );
        
        $id_card=(int)$request->id_card;
        $cant_servicePort=$request->cant_puertos*64;
        
        try {
            if ($validate) {
                # code...*/
                $target_port = $request->except('_token'); //toma todos los valores excepto el token
                // estamos haciendo que se almacene todo lo que se envia al metodo store
                // return response()->json($target_port);s
                target_port::insert([
                    'olt_id' => $request->olt_id,
                    'ranura_tarjeta' => $request->ranura_tarjeta,
                    'id_card' => $id_card,
                    'VPI' => $request->VPI,
                    'S_VLAN' => $request->S_VLAN,
                    'puerto_inicial' => $request->puerto_inicial,
                    'cant_puertos' => $request->cant_puertos,
                    'C_lan_ini' => $request->C_lan_ini,
                    'created_at' => now()->toDateTime(),
                    'user_email_created' => Auth::user()->email,
                    //'cant_servicePort'=>$cant_servicePort
                ]);
                
                $ultimoId = target_port::latest('id')->first();
                
                $puerto_ini=olt::findOrFail($request->olt_id);

                 for ($i = $puerto_ini->puerto_serv_inicial; $i < $cant_servicePort; $i++) {
                     # code...
                     DB::table('service_port')
                     ->insert([
                         'puerto' => $i,
                         'disponibilidad' => 1,
                         'id_targetPorts' => $ultimoId->id,
                     ]);
                 }

                $respuesta['respuesta'] = array(
                    "title" => "Creación de la tarjeta",
                    "msg" => "Estimado usuario, la tarjeta se ha creado exitosamente",
                    "ruta" => 'TargetPort',
                    "otros" => ""
                );
                return view('mensajes.satisfactorio', $respuesta);
            }
        } 
        catch (\Throwable $th) 
        {
            // throw $th;
            $respuesta['respuesta'] = array(
                "title" => "Creación de olt",
                "msg" => "Estimado usuario, no se ha podido realizar la creación de la tarjeta, intente más tarde.",
                "ruta" => 'TargetPort',
                "otros" => ""
            );
            return view('mensajes.error', $respuesta);

        }

       


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\target_port  $target_port
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        
        $id =(int) $request->id;

        $consulta = DB::table('config_cards')->where('id','=',$id)->get();
        return $consulta;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\target_port  $target_port
     * @return \Illuminate\Http\Response
     */
    public function edit( $idtarget_port)
    {
        //
        $target_port=DB::table('target_ports')
        ->join('olts','target_ports.olt_id','=','olts.id')
        ->join('config_cards','target_ports.id_card','=','config_cards.id')
        ->join('statuses','target_ports.id_status','=','statuses.id')
        ->select('target_ports.*','ip_olt','descripcion','nombre_tarjeta','tipo_target')
        ->where('target_ports.id','=',$idtarget_port)
        ->get();
        
        $datos['olt'] = olt::paginate(5);
        $datos['status'] = status::all()->except([2,3,4]);
        $datos['config_card'] = config_card::get();
        $datos['puertoini']= 0 ;
       
        return view('ports.edit', ['target_port' => $target_port,$datos]);
	
        //return view('ports.edit', $datos);

        // return response()->json($target_port);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\target_port  $target_port
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $idtarget_port)
    {
        //
        $validate = request()->validate(
            [
                'olt_id' => 'required',
                'ranura_tarjeta' => 'required',
                'nombre_tarjeta' => 'required|string|between:3,25',
                'tipo_target' => 'required|string|between:3,25',
                'VPI' => 'required',
                'S_VLAN' => 'required',
                'puerto_inicial' => 'required',
                'cant_puertos' => 'required',
                'cant_ont' => 'required',
                'ID_estatus' => 'required',
                'C_lan_ini' => 'required'
            ]
        );
        try {
            //code...
            if ($validate) {
                # code...
                $target_port = $request->except(['_token', '_method']); //toma todos los valores excepto el token
                target_port::where('id', '=', $idtarget_port)->update([
                    'olt_id' => $request->olt_id,
                    'ranura_tarjeta' => $request->ranura_tarjeta,
                    'nombre_tarjeta' => $request->nombre_tarjeta,
                    'tipo_target' => $request->tipo_target,
                    'VPI' => $request->VPI,
                    'S_VLAN' => $request->S_VLAN,
                    'puerto_inicial' => $request->puerto_inicial,
                    'cant_puertos' => $request->cant_puertos,
                    'puertos' => $request->puerto_inicial * $request->cant_puertos,
                    'cant_ont' => $request->cant_ont,
                    'C_lan_ini' => $request->C_lan_ini,
                    'C_lan' => $request->cant_ont * $request->C_lan_ini,
                    'ID_estatus' => $request->ID_estatus,
                    'updated_at' => now()->toDateTime(),
                    'user_email_updated' => Auth::user()->email,
                ]);
                $target_port = target_port::findOrFail($idtarget_port);
                // return response()->json($target_port);
                $respuesta['respuesta'] = array(
                    "title" => "Creación de Tarjeta y Puertos",
                    "msg" => "Estimado usuario, la Tarjeta y Puertos se ha modificado exitosamente",
                    "ruta" => "TargetPort",
                    "otros" => ""
                );
                return view('mensajes.satisfactorio', $respuesta);
            }

        } catch (\Throwable $th) {
            //throw $th;
            $respuesta['respuesta'] = array(
                "title" => "Creación de olt",
                "msg" => "Estimado usuario, no se ha podido realizar la modificación de Tarjeta y Puerto, intente más tarde.",
                "ruta" => 'aprovicion',
                "otros" => ""
            );
            return view('mensajes.error', $respuesta);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\target_port  $target_port
     * @return \Illuminate\Http\Response
     */
    public function destroy(target_port $target_port)
    {
        //

    }
    public function mostrar()
    {
        // //
        
        // return view('service_Profile.index', $datos);
        $target_port=DB::table('target_ports')
            ->join('config_cards','id_card','=','config_cards.id')
            ->select('target_ports.*','nombre_tarjeta','tipo_target')
            ->paginate(15);

            
        return view('ports.form',['target_port'=>$target_port,]);
    }

}
