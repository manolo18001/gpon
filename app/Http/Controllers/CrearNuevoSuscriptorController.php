<?php

namespace App\Http\Controllers;


use Httpful\Exception\ConnectionErrorException;
use Httpful\Request;
use Illuminate\Http\Request as HttpRequest;



class CrearNuevoSuscriptorController extends Controller
{
    //

  public function store(HttpRequest $request)
  {    
   
    $IMSI=$request->IMSI;
    $chargingType=$request->chargingType;
    $contactTeleNo=$request->contactTeleNo;
    $effectTime=$request->effectTime;
    $homeAreaID=$request->homeAreaID;
    $maxSessNumber=$request->maxSessNumber;
    $payAccount=$request->payAccount;
    $permittedANTYp=$request->permittedANTYp;
    $productID=$request->productID;
    $remoteAddress=$request->remoteAddress;
    $sequenceId=$request->sequenceId;
    $serialNo=$request->serialNo;
    $subscriberID=$request->subscriberID;
    $timeStampSN=$request->timeStampSN;
    
    $url="http://picqa05:8990/mule/services/AP900CrearNuevoSuscriptorAba";
    
    
    $xml='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mul="http://MULECrearNuevoSuscriptorAba/" xmlns:mul1="http://MULECrearNuevoSuscriptorAba">
    <soapenv:Header/>
    <soapenv:Body>
       <mul:CrearNuevoSuscriptor>
          <!--Optional:-->
          <mul:arg0>
            <mul1:IDCLIENTEPIC>BossAprov</mul1:IDCLIENTEPIC>
                   <mul1:IMSI>'.$IMSI.'</mul1:IMSI>
                   <mul1:chargingType>'.$chargingType.'</mul1:chargingType>
                   <mul1:contactTeleNo>'.$contactTeleNo.'</mul1:contactTeleNo>
                   <mul1:effectTime>'.$effectTime.'</mul1:effectTime>
                   <mul1:homeAreaID>'.$homeAreaID.'</mul1:homeAreaID>
                   <mul1:maxSessNumber>'.$maxSessNumber.'</mul1:maxSessNumber>
                   <mul1:payAccount>'.$payAccount.'</mul1:payAccount>
                   <mul1:permittedANTYpe>'.$permittedANTYp.'</mul1:permittedANTYpe>
                   <mul1:productID>'.$productID.'</mul1:productID>
                   <mul1:remoteAddress>'.$remoteAddress.'</mul1:remoteAddress>
                   <mul1:sequenceId>'.$sequenceId.'</mul1:sequenceId>
                   <mul1:serialNo>'.$serialNo.'</mul1:serialNo>
                   <mul1:subscriberID>'.$subscriberID.'</mul1:subscriberID>
                   <mul1:timeStampSN>'.$timeStampSN.'</mul1:timeStampSN>
          </mul:arg0>
       </mul:CrearNuevoSuscriptor>
      </soapenv:Body>
     </soapenv:Envelope>';
    
    
    try
    {
      $response = Request::post($url)->expectsXml()->body($xml)->send();

    }catch(ConnectionErrorException $e)
    {
      echo "error, verifica los datos que estas ingresando ";
    }
    
    return $response;  
        
  }
}


