<?php

namespace App\Http\Controllers;

use App\aptonombre;
use Illuminate\Http\Request;

class AptonombreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\aptonombre  $aptonombre
     * @return \Illuminate\Http\Response
     */
    public function show(aptonombre $aptonombre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\aptonombre  $aptonombre
     * @return \Illuminate\Http\Response
     */
    public function edit(aptonombre $aptonombre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\aptonombre  $aptonombre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, aptonombre $aptonombre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\aptonombre  $aptonombre
     * @return \Illuminate\Http\Response
     */
    public function destroy(aptonombre $aptonombre)
    {
        //
    }
}
