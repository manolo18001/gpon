<?php

namespace App\Http\Controllers;

use App\elemento_detalle;
use Illuminate\Http\Request;

class ElementoDetalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\elemento_detalle  $elemento_detalle
     * @return \Illuminate\Http\Response
     */
    public function show(elemento_detalle $elemento_detalle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\elemento_detalle  $elemento_detalle
     * @return \Illuminate\Http\Response
     */
    public function edit(elemento_detalle $elemento_detalle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\elemento_detalle  $elemento_detalle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, elemento_detalle $elemento_detalle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\elemento_detalle  $elemento_detalle
     * @return \Illuminate\Http\Response
     */
    public function destroy(elemento_detalle $elemento_detalle)
    {
        //
    }
}
