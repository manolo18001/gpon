<?php

namespace App\Http\Controllers;
use App\Manga;
use App\Mangas2;
use App\Odf;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class MangaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $elemento = [
            ['id' => 1, 'elemento' => 'ODF'],
            ['id' => 2, 'elemento' => 'Manga'],
            ['id' => 'n2', 'elemento' => 'Manga 1']
            // ['id' => 'n3', 'elemento' => 'Manga 2'],
            // ['id' => 'n4', 'elemento' => 'Manga 3']
        ];

        $cable = [
            ['id' => 1, 'cable' => 'Cable principal'],
            ['id' => 2, 'cable' => 'Cable secundario '],
            ['id' => 3, 'cable' => 'Buffer']
        ];
        return view('manga.manga',compact('elemento','cable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
        'previo'=>'required',
        'previo2'=>'required',
        'nombre'=>'required',
        'perdida'=>'required|numeric',
        'cable'=>'required',
        'cantidad'=>'required',

        ]);
           // probando
            try{
            if($request->previo=='Mangas'){
               // $test1 = Manga::where("id","=",$request->previo2)->select('mangas.id')->get();
               // return $test1;
                $reserves = DB::table('mangas2')
                ->where('id_manga','=', $request->previo2)
                ->select(['id_manga'])
                ->count(); 
                $reserves2 = $reserves+1;  
               //  return $reserves;
               //  if($test1>'0'){
                    DB::table('mangas2')
                    ->insert([
                   'previo'=>$request->previo, 
                   'id_manga' => $request->previo2,
                    'manga2' => $request->nombre,
                    'perdida' => $request->perdida,
                    'tipo_cable' => $request->cable,
                    'cant_saliente' => $request->cantidad,   
                    'nivel' =>  $reserves2,   
                    'created_at' => now()->toDateTime(),
                    'user_email_created' => Auth::user()->email,
                    ]);
            }   
            //nueva prueba
            // if($request->previo == 'n2'){

            //     return $request->previo;

            // }
            // fin prueba
            else{
               Manga::insert([
                   'previo'=>$request->previo,
                   'mangas' => $request->nombre,
                   'perdida' => $request->perdida,
                   'cable' => $request->cable,
                   'id_odf'=>$request->previo2,
                   'cantidad' => $request->cantidad,        
                   'created_at' => now()->toDateTime(),
                   'user_email_created' => Auth::user()->email
               ]);
            }

            $respuesta['respuesta'] = array(
                "title" => "Datos de ventas",
                "msg" => "Estimado usuario, la manga se ha creado exitosamente",
                "ruta" => 'manga',
                "otros" => ""
            );
            return view('mensajes.satisfactorio', $respuesta);
        
    } catch (\Throwable $th) {
        //throw $th;
    }
            
     
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_manga)
    {
        //
        $manga= Manga::findOrFail($id_manga);
        $id_odf=json_decode($manga,true);
        $odf=Odf::findOrFail($id_odf['id_odf']);
        return view('manga.edit',['manga'=>$manga,'odf'=>$odf]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function mostrar()
    {
        $mangas = Manga::paginate(100);
        return view('manga.form',['mangas'=>$mangas]);
        //
    }

    public function mostrar2()
    {
        $mangas1 = Manga::select('*')
        ->get();
        
        return $mangas1;
        //
    }

    
    public function mostrar3()
    {
        $mangas1 = DB::table('mangas2')
        ->select('*')
        ->where('nivel_2','=','n2')
        ->get();
        
        return $mangas1;
        //
    }
}
