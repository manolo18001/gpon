<?php

namespace App\Http\Controllers;


use App\central;
use App\olt;
use App\Installggpm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class InstallggpmController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $central = central::get();
        return view('installggpm.installggpm', compact('central'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validate = request()->validate(
                [
                'central_id' => 'required',
                'olt_id' => 'required',
                'id_targetPort' => 'required',
                'puerto_id' => 'required',
                'odf_id' => 'required',

                ]
            );
        try {
            //code...
            if ($validate) {
                # code...
                Installggpm::insert([
                    'central_id' => $request->central_id,
                    'olt_id' => $request->olt_id,
                    'id_targetPort' => $request->id_targetPort,
                    'puerto_id' => $request->puerto_id,
                    'odf_id' => $request->odf_id,
                    'created_at' => now()->toDateTime(),
                    // 'user_email_created' => Auth::user()->email,
                ]);
                $respuesta['respuesta'] = array(
                    "title" => "Creación de ont",
                    "msg" => "Estimado usuario,Instalación GGPM se ha creado exitosamente",
                    "ruta" => 'Installggpm',
                    "otros" => ""
                );
                return view('mensajes.satisfactorio', $respuesta);
            }
        } catch (\Throwable $th) {
            //throw $th;
            $respuesta['respuesta'] = array(
                "title" => "Creación de ont",
                "msg" => "Estimado usuario, no se ha podido realizar la creación del Instalación GGPM, intente más tarde.",
                "ruta" => 'Installggpm',
                "otros" => ""
            );
            return view('mensajes.error', $respuesta);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function mostrar()
    {
        // $datos['Installggpm'] = DB::select('SELECT * FROM olts where central_id = ?', [$request->central_id]);
// return response()->json($datos);
        return view('installggpm.form');
    }

    // public function mostrar2(Request $request)
    // {
    //     $id = $request->id;
    //     $central = olt::where('central_id','=',$request->id)->select('olts.Nombre_elemento')->get();//table('olts')->where('central_id','=',$request->id);//table('olts')->where('central_id','=',$request->id)->select('*');


    //     return $central;
    // }

    public function getOlt(Request $request)
    {
        //
        if ($request->ajax()) {
            # code...
            // $ciudades =  olt::where('central_id', $request->central_id)->get();

            $sfps =  DB::select('SELECT * FROM olts where central_id = ?', [$request->central_id]);
            foreach ($sfps as $sfp) {
                # code...
                // $urbanizacionArray[$sfp->id] = $sfp->nameSfp;
                $urbanizacionArray[$sfp->id] = $sfp->Nombre_elemento;
            }
            return response()->json($urbanizacionArray);
        }
    }



    public function getOdf(Request $request)
    {
        //
        if ($request->ajax()) {
            # code...
            // $ciudades =  olt::where('central_id', $request->central_id)->get();

            $sfps =  DB::select('SELECT * FROM odfs where central = ?', [$request->central_id]);
            foreach ($sfps as $sfp) {
                # code...
                // $urbanizacionArray[$sfp->id] = $sfp->nameSfp;
                $urbanizacionArray[$sfp->id] = $sfp->odf;
            }
            return response()->json($urbanizacionArray);
        }
    }


    public function getTarget(Request $request)
    {
        //
        if ($request->ajax()) {
            # code...
            // $ciudades =  olt::where('central_id', $request->central_id)->get();

            $sfps =  DB::select('SELECT id,"S_VLAN",olt_id FROM target_ports where olt_id = ?', [$request->olt_id]);
            foreach ($sfps as $sfp) {
                # code...
                // $urbanizacionArray[$sfp->id] = $sfp->nameSfp;
                $urbanizacionArray[$sfp->id] = $sfp->S_VLAN;
            }
            return response()->json($urbanizacionArray);
        }
    }

    public function getPort(Request $request)
    {
        //
        if ($request->ajax()) {
            # code...
            // $ciudades =  olt::where('central_id', $request->central_id)->get();

            $sfps =  DB::select('SELECT id,"id_targetPort",puerto FROM puertos where "id_targetPort" = ?', ['10']);
            foreach ($sfps as $sfp) {
                # code...
                // $urbanizacionArray[$sfp->id] = $sfp->nameSfp;
                $urbanizacionArray[$sfp->id] = $sfp->puerto;
            }
            return response()->json($urbanizacionArray);
        }
    }

}

