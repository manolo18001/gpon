<?php

namespace App\Services;

use App\sale;
use App\estado;
use App\central;

use App\municipio;
use App\parroquia;
use App\urbanizacion;
use App\ciudad;
use Illuminate\Http\Request;

class estados
{
    public function getEstados(){
        $estados = estado::get();
        $estadosArray[''] = 'Seleccione un estado';
        foreach ($estados as $estado){
            # code...
            $estadosArray[$estado->id] = $estado->estado;
        }
        return $estadosArray;
    }
    // public function central(){
    //     $central = central::get();
    //     $centralArray[''] = 'Seleccione';
    //     foreach ($central as $central){
    //         # code...
    //         $centralArray[$central->id] = $central->name_central;
    //     }
    //     return $centralArray;
    // }


    public function getCentral(){
        $central = central::get();
        $centralArray[''] = 'Seleccione';
        foreach ($central as $central){
            # code...
            $centralArray[$central->id] = $central->name_central;
        }
        return $centralArray;
    }
}
