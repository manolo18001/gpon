function campoInvalido(nombreCampo){

	$("#"+nombreCampo).css("border-color","red");
	$("#"+nombreCampo).parent().addClass("has-error has-feedback");
	$("#b").remove();
	$("#m").remove();
	$("#"+nombreCampo).parent().append('<span id="m" class="glyphicon glyphicon-remove form-control-feedback"></span>');
	return false;
}
function campoValido(nombreCampo){
	$("#smp").remove();
	$("#"+nombreCampo).css("border-color","green");
	$("#"+nombreCampo).parent().addClass("has-success has-feedback");
	$("#m").remove();
	$("#b").remove();
	$("#"+nombreCampo).parent().append('<span id="b" class="glyphicon glyphicon-ok form-control-feedback" style="color:green;"></span>');

}
function selectInvalido(nombreCampo){

	$("#"+nombreCampo).css("border-color","red");
	$("#"+nombreCampo).parent().addClass("has-error has-feedback");
	//$("#b").remove();
	//$("#m").remove();
	//$("#"+nombreCampo).parent().append('<span id="m" class="glyphicon glyphicon-remove form-control-feedback"></span>');
}
function selectValido(nombreCampo){
	$("#smp").remove();
	$("#"+nombreCampo).css("border-color","green");
	$("#"+nombreCampo).parent().addClass("has-success has-feedback");
	//$("#m").remove();
	//$("#b").remove();
	//$("#"+nombreCampo).parent().append('<span id="b" class="glyphicon glyphicon-ok form-control-feedback" style="color:green;"></span>');
}
function validar(opcion){
	switch(opcion){
		case "prefijo_rif":
			var prefijo = $("#prefijo_rif").val();
			if(prefijo == ""){
				$("#smp").remove();
				$("#prefijo_rif").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Seleccione</small></p>");
				selectInvalido("prefijo_rif");
				return false;
				}else{
					selectValido("prefijo_rif");
					return true;
				}

		break;
		case "form_rif":
			var rif = $("#form_rif").val();
			var expr = /^[0-9]{6,9}$/;

			if(rif == ""){$("#smp").remove();$("#form_rif").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_rif");	return false;}
			if(rif.length < 6){$("#smp").remove();$("#form_rif").parent().append("<p id='smp' style='color:red; font-style : normal;'><small >Ingrese mínimo 6 caracteres Ej: 123456789</small></p>");campoInvalido("form_rif");	return false;}
			if (expr.test(rif) == false){$("#smp").remove();$("#form_rif").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>su número no cumple con el formato Ej: J12345678</small></p>");campoInvalido("form_rif");	return false;}

			if (rif.length >= 6 ){
				var check;
				var datos =  { "opcion" : "rif",	"prefijo" : $('#prefijo_rif').val(), "rif" : rif };
				$.ajax({
				data: datos,
				url: '../core/Controllers/verificarDatosController.php',
				type: 'post',
				async: false,
				success: function(response){
					check = response;
						if(response == "existe"){
							$("#smp").remove();
							$('#form_rif').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El RIF ya se encuantra registrado</small></p>");
							campoInvalido("form_rif");
							$("#form_rif").focus();
							return false;
						}
					}
				});

				 if(check == "existe"){
						$("#smp").remove();
						$('#form_rif').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El RIF ya se encuantra registrado</small></p>");
						campoInvalido("form_rif");
						 return false;
					 }else{campoValido("form_rif");	return true;}
				}

		break;
		case "prefijo_ced":
			var prefijo = $("#prefijo_ced").val();
			if(prefijo == ""){
				$("#smp").remove();
				$("#prefijo_ced").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Seleccione</small></p>");
				selectInvalido("prefijo_ced");
				return false;}
				else{
					selectValido("prefijo_ced");
					return true;
				}

		break;
		case "prefijo_tlf_contacto":
			var prefijo = $("#prefijo_tlf_contacto").val();
			if(prefijo == ""){
				$("#smp").remove();
				$("#prefijo_tlf_contacto").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Seleccione</small></p>");
				selectInvalido("prefijo_tlf_contacto");
				return false;
				}else{
					selectValido("prefijo_tlf_contacto");
					return true;
				}

		break;
		case "prefijo_tlf_movil":
			var prefijo = $("#prefijo_tlf_movil").val();
			if(prefijo == ""){
				$("#smp").remove();
				$("#prefijo_tlf_movil").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Seleccione</small></p>");
				selectInvalido("prefijo_tlf_movil");
				return false;
				}else{
					selectValido("prefijo_tlf_movil");
					return true;
				}

		break;
		case "form_cedula":
			var cedula = $("#form_cedula").val();
			var expr = /^[0-9]{5,9}$/;
			if(cedula == ""){$("#smp").remove();$("#form_cedula").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_cedula");	return false;}
			if(cedula.length < 5){$("#smp").remove();$("#form_cedula").parent().append("<p id='smp' style='color:red; font-style : normal;'><small >Ingrese mínimo 5 caracteres Ej: 12345678</small></p>");campoInvalido("form_cedula");	return false;}
			if (expr.test(cedula) == false){$("#smp").remove();$("#form_cedula").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>su número no cumple con el formato Ej: V12345678</small></p>");campoInvalido("form_cedula");	return false;}
			if (cedula.length >= 5 ){
				// var datos =  { "opcion" : "cedula",	"prefijo" : $('#prefijo_ced').val(), "cedula" : cedula };
				// var check;
				// 		$.ajax({
				// 			data: datos,
				// 			url: '../core/Controllers/verificarDatosController.php',
				// 			type: 'post',
				// 			async: false,
				// 			success: function(response){
				// 					check = response;
				// 					if(response == "existe"){
				// 						$("#smp").remove();
				// 						$('#form_cedula').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>La cédula ya se encuentra registrada</small></p>");
				// 						campoInvalido("form_cedula");
				// 						$("#form_cedula").focus();
				// 						return false;
				// 					}
				// 				}
				// 			}).always(function(){
        		// 				console.log(check);
    			// 			});
				// 	 if(check == "existe"){
				// 		 $("#smp").remove();
				// 		$('#form_cedula').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>La cédula ya se encuentra registrada</small></p>");
				// 		campoInvalido("form_cedula");
				// 		 return false;
				// 	 }else{ campoValido("form_cedula"); return true;}
                return true;
				}

		break;
		case "form_cedula_oc":
			var cedula = $("#form_cedula_oc").val();
			var expr = /^[0-9]{5,9}$/;
			if(cedula == ""){$("#smp").remove();$("#form_cedula_oc").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_cedula_oc");	return false;}
			if(cedula.length < 5){$("#smp").remove();$("#form_cedula_oc").parent().append("<p id='smp' style='color:red; font-style : normal;'><small >Ingrese mínimo 5 caracteres Ej: 12345678</small></p>");campoInvalido("form_cedula_oc");	return false;}
			if (expr.test(cedula) == false){$("#smp").remove();$("#form_cedula_oc").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>su número no cumple con el formato Ej: V12345678</small></p>");campoInvalido("form_cedula_oc");	return false;}
			if (cedula.length >= 5 ){campoValido("form_cedula_oc");	}

		break;
		case "form_nombres":
			var nombres = $("#form_nombres").val();
			var expr = /^\s*$/;
			if(nombres == ""){$("#smp").remove();$("#form_nombres").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_nombres");	return false;}
			if(expr.test(nombres) != false){$("#smp").remove();$("#form_nombres").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Nombre no válido</small></p>");campoInvalido("form_nombres");	return false;}
			else {campoValido("form_nombres");	return true;}
		break;
		case "form_apellidos":
			var apellidos = $("#form_apellidos").val();
			var expr = /^\s*$/;
			if(apellidos == ""){$("#smp").remove();$("#form_apellidos").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_apellidos");	return false;}
			if(expr.test(apellidos) != false){$("#smp").remove();$("#form_apellidos").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Apellido no válido</small></p>");campoInvalido("form_apellidos");	return false;}
			else {campoValido("form_apellidos");	return true;}
		break;
		case "form_tlf_contacto":
			var contacto = $("#form_tlf_contacto").val();
			var expr = /^[0-9]{7}$/;
			if(contacto == ""){$("#smp").remove();$("#form_tlf_contacto").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_tlf_contacto");	return false;}
			if (expr.test(contacto) == false){$("#smp").remove();$("#form_tlf_contacto").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Ingrese su número con el formato Ej: 1234567 </small></p>");campoInvalido("form_tlf_contacto");	return false;}
			else{campoValido("form_tlf_contacto");	 return true;}
		break;
		case "form_tlf_movil":
			var movil = $("#form_tlf_movil").val();
			var expr = /^[0-9]{7}$/;
			if(movil == ""){$("#smp").remove();$("#form_tlf_movil").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_tlf_movil");	return false;}
			if (expr.test(movil) == false){$("#smp").remove();$("#form_tlf_movil").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Ingrese su número con el formato Ej: 1234567 </small></p>");campoInvalido("form_tlf_movil");	return false;}
			else{campoValido("form_tlf_movil");	 return true;}
		break;
		case "form_correo_electronico":
			var correo = $("#form_correo_electronico").val();
			var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
			if(correo == ""){$("#smp").remove();$("#form_correo_electronico").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_correo_electronico");	return false;}
			if (expr.test(correo) == false){$("#smp").remove();$("#form_correo_electronico").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Ingrese una dirección de correo con el formato correo@dominio.com</small></p>");campoInvalido("form_correo_electronico");	return false;}
			// var check;
			// var datos =  { "opcion" : "correo", "correo" : correo };
			// $.ajax({
			// 	data: datos,
			// 	url: '../core/Controllers/verificarDatosController.php',
			// 	type: 'post',
			// 	async: false,
			// 	success: function(response){
			// 			check = response;
			// 			//$('#form_correo_electronico').parent().append(response);
			// 			if(response == "existe"){
			// 				$("#smp").remove();
			// 				$('#form_correo_electronico').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El correo electrónico ingresado ya se encuentra registrado</small></p>");
			// 				campoInvalido("form_correo_electronico");
			// 				$("#form_correo_electronico").focus();
			// 				return false;
			// 			}
			// 		}
			// });
			// if(check == "existe"){
			// 		$("#smp").remove();
			// 		$('#form_correo_electronico').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El correo electrónico ingresado ya se encuentra registrado</small></p>");
			// 		campoInvalido("form_correo_electronico");
			// 		return false;
			// }else{
			// 	campoValido("form_correo_electronico");
			// 	return true;
			// 	}
return true;
		break;
		case "form_correo_electronico_r":
			var correo = $("#form_correo_electronico_r").val();
			var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
			if(correo == ""){$("#smp").remove();$("#form_correo_electronico_r").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_correo_electronico_r");	return false;}
			if (expr.test(correo) == false){$("#smp").remove();$("#form_correo_electronico_r").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Ingrese una dirección de correo con el formato correo@dominio.com</small></p>");campoInvalido("form_correo_electronico_r");	return false;}
			else{campoValido("form_correo_electronico_r");	return true;}
		break;
		case "form_correo_electronico_m":
			var correo = $("#form_correo_electronico_m").val();
			var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
			if(correo == ""){$("#smp").remove();$("#form_correo_electronico_m").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_correo_electronico_m");	return false;}
			if (expr.test(correo) == false){$("#smp").remove();$("#form_correo_electronico_m").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Ingrese una dirección de correo con el formato correo@dominio.com</small></p>");campoInvalido("form_correo_electronico_m");	return false;}
			else{campoValido("form_correo_electronico_m"); return true;	}
		break;
		case "form_razon_social":
			var nombres = $("#form_razon_social").val();
			var expr = /^\s*$/;
			if(nombres == ""){$("#smp").remove();$("#form_razon_social").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_razon_social");	return false;}
			if(expr.test(nombres) != false){$("#smp").remove();$("#form_razon_social").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Nombre no válido</small></p>");campoInvalido("form_razon_social");	return false;}
			else {campoValido("form_nombres");	return true;}
		break;
		case "form_correo_empresa":
			var correo = $("#form_correo_empresa").val();
			var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
			if(correo == ""){$("#smp").remove();$("#form_correo_empresa").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_correo_empresa");	return false;}
			if (expr.test(correo) == false){$("#smp").remove();$("#form_correo_empresa").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Ingrese una dirección de correo con el formato correo@dominio.com</small></p>");campoInvalido("form_correo_empresa");	return false;}

			var datos =  { "opcion" : "correo_empresa", "correo" : correo };
			var check;
			$.ajax({
				data: datos,
				url: '../core/Controllers/verificarDatosController.php',
				type: 'post',
				async: false,
				success: function(response){
						check = response;
						//$('#form_correo_electronico').parent().append(response);
						if(response == "existe"){
							$("#smp").remove();
							$('#form_correo_empresa').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El correo electrónico ingresado ya se encuentra registrado </small></p>");
							campoInvalido("form_correo_empresa");
							return false;
						}
					}
				});
				if(check == "existe"){
						$("#smp").remove();
						$('#form_correo_empresa').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El correo electrónico ingresado ya se encuentra registrado </small></p>");
						campoInvalido("form_correo_empresa");
						 return false;
					 }else{ campoValido("form_correo_empresa"); return true;}

		break;
		case "form_correo_empresarial":
			var correo = $("#form_correo_empresarial").val();
			var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
			if(correo == ""){$("#smp").remove();$("#form_correo_empresarial").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_correo_empresarial");	return false;}
			if (expr.test(correo) == false){$("#smp").remove();$("#form_correo_empresarial").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Ingrese una dirección de correo con el formato correo@dominio.com</small></p>");campoInvalido("form_correo_empresarial");	return false;}
			var correo2 = $("#form_correo_empresa").val();
			if (correo == correo2){
					$("#smp").remove();
					$("#form_correo_empresarial").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>El correo empresarial debe ser diferente al correo de la empresa</small></p>");campoInvalido("form_correo_empresarial");
					return false;
				}
			var check;
			var datos =  { "opcion" : "correo_empresarial", "correo" : correo };
			$.ajax({
				data: datos,
				url: '../core/Controllers/verificarDatosController.php',
				type: 'post',
				async: false,
				success: function(response){
					check = response;
						//$('#form_correo_electronico').parent().append(response);
						if(response == "existe"){
							$("#smp").remove();
							$('#form_correo_empresarial').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El correo electrónico ingresado ya se encuentra registrado </small></p>");
							campoInvalido("form_correo_empresarial");
							return false;
						}
					}
				});
			if(check == "existe"){
					$("#smp").remove();
					$('#form_correo_empresarial').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El correo electrónico ingresado ya se encuentra registrado </small></p>");
					campoInvalido("form_correo_empresarial");
					return false;
					 }else{ campoValido("form_correo_empresarial"); return true;}
		break;
		case "form_confirm_correo":
			var correo = $("#form_confirm_correo").val();
			var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
			if(correo == ""){$("#smp").remove();$("#form_confirm_correo").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_confirm_correo");	return false;}
			if (expr.test(correo) == false){$("#smp").remove();$("#form_confirm_correo").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Ingrese una dirección de correo con el formato correo@dominio.com</small></p>");campoInvalido("form_confirm_correo");	return false;}
			else{campoValido("form_confirm_correo");	}
			var correo2 = $("#form_correo_electronico").val();
			if (correo != correo2){
					$("#smp").remove();$("#form_confirm_correo").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Los correos no coinciden </small></p>");campoInvalido("form_confirm_correo");
					$("#form_correo_electronico").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Los correos no coinciden </small></p>");campoInvalido("form_correo_electronico");
					return false;
				}
			else{
				campoValido("form_correo_electronico");
				campoValido("form_confirm_correo");
			}
		break;
		case "form_confirm_correo_n":
			var correo = $("#form_confirm_correo_n").val();
			var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
			if(correo == ""){$("#smp").remove();$("#form_confirm_correo_n").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_confirm_correo_n");	return false;}
			if (expr.test(correo) == false){$("#smp").remove();$("#form_confirm_correo_n").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Ingrese una dirección de correo con el formato correo@dominio.com</small></p>");campoInvalido("form_confirm_correo_n");	return false;}
			else{campoValido("form_confirm_correo_n");	}

			var correo2 = $("#form_nuevo_correo").val();
			if (correo != correo2){
					$("#smp").remove();$("#form_confirm_correo_n").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Las correos no coinciden </small></p>");campoInvalido("form_confirm_correo_n");
					$("#form_nuevo_correo").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Los correos no coinciden </small></p>");campoInvalido("form_nuevo_correo");
					return false;
				}
			else{
				campoValido("form_correo_electronico_n");
				campoValido("form_nuevo_correo");
			}
		break;
		case "form_contrasena":
			var pass = $("#form_contrasena").val();
			var expr = /^(?!.*(.)\1)[A-Z]+(?=.*\d)(?=.*[¿:;!\\\{\}\[\]#*$”?\=!\)\(/°%&\.,+_\-])(?=.*[a-z])\S{7,25}$/;

			if(pass == ""){$("#smp").remove();$("#form_contrasena").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_contrasena");	return false;}

			if (expr.test(pass) == false){$("#smp").remove();$("#form_contrasena").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>La contraseña no posee el formato necesario</small></p>");campoInvalido("form_contrasena");	return false;}
			else{campoValido("form_contrasena");	return true;}
		break;
		case "form_confirm_contrasena":
			var pass = $("#form_confirm_contrasena").val();
			var expr = /^(?!.*(.)\1)[A-Z]+(?=.*\d)(?=.*[¿:;!\\\{\}\[\]#*$”?\=!\)\(/°%&\.,+_\-])(?=.*[a-z])\S{7,25}$/;

			if(pass == ""){$("#smp").remove();$("#form_confirm_contrasena").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_confirm_contrasena");	return false;}

			if (expr.test(pass) == false){$("#smp").remove();$("#form_confirm_contrasena").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>La contraseña no posee el formato necesario</small></p>");campoInvalido("form_confirm_contrasena");	return false;}
			else{campoValido("form_confirm_contrasena");	}

			var pass2 = $("#form_contrasena").val();
			if (pass != pass2){
					$("#smp").remove();$("#form_confirm_contrasena").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No coincide con el campo contraseña </small></p>");campoInvalido("form_confirm_contrasena");
					//$("#form_contrasena").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No coincide con el campo contraseña </small></p>");campoInvalido("form_contrasena");
					return false;
				}
			else{

				//campoValido("form_contrasena");
				campoValido("form_confirm_contrasena");
				return true;
			}

		break;

		case "form_contrasena_actual":
			var pass = $("#form_contrasena_actual").val();
			var expr =/^(?!.*(.)\1)[A-Z]+(?=.*\d)(?=.*[¿:;!\\\{\}\[\]#*$”?\=!\)\(/°%&\.,+_\-])(?=.*[a-z])\S{7,25}$/;

			if(pass == ""){$("#smp").remove();$("#form_contrasena_actual").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_contrasena_actual");	return false;}

			if (expr.test(pass) == false){$("#smp").remove();$("#form_contrasena_actual").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>La contraseña no posee el formato necesario</small></p>");campoInvalido("form_contrasena_actual");	return false;}
			else{campoValido("form_contrasena_actual");	return true;}
		break;

		case "form_usuario":
			var usuario = $("#form_usuario").val();
			var expr = /^[a-zA-Z0-9\-\_\.]{8,}/g;
			if(usuario == ""){
				$("#smp").remove();
				$("#form_usuario").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");
				campoInvalido("form_usuario");
				return false;
				}
			if(expr.test(usuario) != true){
				$("#smp").remove();
				$("#form_usuario").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Ingrese un usuario con el formato: al menos 8 caracteres, letras, números y puntos</small></p>");
				campoInvalido("form_usuario");
				return false;
				}
			if(usuario.length >=8){
				var check;
				var datos =  { "opcion" : "usuario", "usuario" : usuario };
				  $.ajax({
					data: datos,
					url: '../core/Controllers/verificarDatosController.php',
					type: 'post',
					async: false,
					success: function(response){
							check = response;
							if(response == "existe"){
								$("#smp").remove();
								$('#form_usuario').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El usuario ingresado ya existe</small></p>");
								campoInvalido("form_usuario");
								$("#form_usuario").focus();
							}
						}
					});
			}
			if(check == "existe"){
						$("#smp").remove();
						$('#form_usuario').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El usuario ingresado ya existe</small></p>");
						campoInvalido("form_cedula");
						 return false;
					 }else{ campoValido("form_cedula"); return true;}

		break;
		case "form_respuesta_1":
			var respuesta = $("#form_respuesta_1").val();
			var expr = /^\s*$/;
			if(respuesta == ""){
					$("#smp").remove();
					$("#form_respuesta_1").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");
					campoInvalido("form_respuesta_1");
					return false;
				}
			if(expr.test(respuesta) != false){

					$("#smp").remove();
					$("#form_respuesta_1").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No válido</small></p>");
					campoInvalido("form_respuesta_1");
					return false;
				}

				r2 = $('#form_respuesta_2').val();
				if (respuesta == r2){
					$("#smp").remove();
					$("#form_respuesta_1").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No se pueden repetir las respuestas</small></p>");
					$("#smp").remove();
					$("#form_respuesta_2").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No se pueden repetir las respuestas</small></p>");

					campoInvalido("form_respuesta_1");
					campoInvalido("form_respuesta_2");
					return false;
				}
				r3 = $('#form_respuesta_3').val();
				if (respuesta == r3){
					$("#smp").remove();
					$("#form_respuesta_1").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No se pueden repetir las respuestas</small></p>");
					$("#smp").remove();
					$("#form_respuesta_3").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No se pueden repetir las respuestas</small></p>");

					campoInvalido("form_respuesta_1");
					campoInvalido("form_respuesta_3");
					return false;
				}
				r4 = $('#form_respuesta_4').val();
				if (respuesta == r4){
					$("#smp").remove();
					$("#form_respuesta_1").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No se pueden repetir las respuestas</small></p>");
					$("#smp").remove();
					$("#form_respuesta_4").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No se pueden repetir las respuestas</small></p>");

					campoInvalido("form_respuesta_1");
					campoInvalido("form_respuesta_4");
					return false;
				}
				else {campoValido("form_respuesta_1");	}
		break;
		case "form_respuesta_2":
			var respuesta = $("#form_respuesta_2").val();
			var expr = /^\s*$/;
			if(respuesta == ""){$("#smp").remove();$("#form_respuesta_2").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_respuesta_2");	return false;}
			if(expr.test(respuesta) != false){$("#smp").remove();$("#form_respuesta_2").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No válido</small></p>");campoInvalido("form_respuesta_2");	return false;}
			r3 = $('#form_respuesta_3').val();
				if (respuesta == r3){
					$("#smp").remove();
					$("#form_respuesta_2").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No se pueden repetir las respuestas</small></p>");
					$("#form_respuesta_3").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No se pueden repetir las respuestas</small></p>");

					campoInvalido("form_respuesta_2");
					campoInvalido("form_respuesta_3");
					return false;
				}
				r4 = $('#form_respuesta_4').val();
				if (respuesta == r4){
					$("#smp").remove();
					$("#form_respuesta_2").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No se pueden repetir las respuestas</small></p>");
					$("#form_respuesta_4").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No se pueden repetir las respuestas</small></p>");

					campoInvalido("form_respuesta_2");
					campoInvalido("form_respuesta_4");
					return false;
				}
			else {campoValido("form_respuesta_2");	}
		break;
		case "form_respuesta_3":
			var respuesta = $("#form_respuesta_3").val();
			var expr = /^\s*$/;
			if(respuesta == ""){$("#smp").remove();$("#form_respuesta_3").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_respuesta_3");	return false;}
			if(expr.test(respuesta) != false){$("#smp").remove();$("#form_respuesta_3").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No válido</small></p>");campoInvalido("form_respuesta_3");	return false;}
			r4 = $('#form_respuesta_4').val();
				if (respuesta == r4){
					$("#smp").remove();
					$("#form_respuesta_3").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No se pueden repetir las respuestas</small></p>");
					$("#form_respuesta_4").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No se pueden repetir las respuestas</small></p>");

					campoInvalido("form_respuesta_3");
					campoInvalido("form_respuesta_4");
					return false;
				}
			else {campoValido("form_respuesta_3");	}
		break;
		case "form_respuesta_4":
			var respuesta = $("#form_respuesta_4").val();
			var expr = /^\s*$/;
			if(respuesta == ""){$("#smp").remove();$("#form_respuesta_4").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_respuesta_4");	return false;}
			if(expr.test(respuesta) != false){$("#smp").remove();$("#form_respuesta_4").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No válido</small></p>");campoInvalido("form_respuesta_4");	return false;}

			else {campoValido("form_respuesta_4");	}
		break;
		case "imagen":
			var imagen = $('input:radio[name=imagen]:checked').val();

			if(imagen == null){
				$("#caja_imagenes").css("border-color","red");
				$("#caja_imagenes").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Debe seleccionar una imagen</small></p>");
				campoInvalido("caja_imagenes");	return false;
				//alert("Debe seleccionar una imagen");
				return false;
			}
		break;

		case "form_respuesta1":
			var respuesta = $("#form_respuesta1").val();
			var expr = /^\s*$/;
			if(respuesta == ""){$("#smp").remove();$("#form_respuesta1").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_respuesta1");	return false;}
			if(expr.test(respuesta) != false){$("#smp").remove();$("#form_respuesta1").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No válido</small></p>");campoInvalido("form_respuesta1");	return false;}
			campoValido("form_respuesta1");
		break;
		case "form_respuesta2":
			var respuesta = $("#form_respuesta2").val();
			var expr = /^\s*$/;
			if(respuesta == ""){$("#smp").remove();$("#form_respuesta2").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_respuesta2");	return false;}
			if(expr.test(respuesta) != false){$("#smp").remove();$("#form_respuesta2").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>No válido</small></p>");campoInvalido("form_respuesta2");	return false;}
			campoValido("form_respuesta2");
		break;
		case "form_codigo_rec":
			var codigo = $("#form_codigo_rec").val();
			var expr = /^\s*$/;
			if(codigo == ""){$("#smp").remove();$("#form_codigo_rec").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Este campo es obligatorio</small></p>");campoInvalido("form_codigo_rec");	return false;}
			if(expr.test(codigo) != false){$("#smp").remove();$("#form_codigo_rec").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Código no válido</small></p>");campoInvalido("form_codigo_rec");	return false;}
			else {campoValido("form_codigo_rec");	}
		break;

		case "form_pregunta_1":
			var pregunta = $('#select_pregunta_1').val();
			if(pregunta == "Selecciona una pregunta"){
				$("#smp").remove();
				$("#select_pregunta_1").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Seleccione una pregunta</small></p>");
				selectInvalido("select_pregunta_1");
				return false;
			}
			else{
					selectValido("select_pregunta_1");
					return true;
				}
		break;

		case "form_pregunta_2":
			var pregunta = $('#select_pregunta_2').val();
			if(pregunta == "Selecciona una pregunta"){
				$("#smp").remove();
				$("#select_pregunta_2").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Seleccione una pregunta</small></p>");
				selectInvalido("select_pregunta_2");
				return false;
			}
			else{
					selectValido("select_pregunta_2");
					return true;
				}
		break;
		case "form_pregunta_3":
			var pregunta = $('#select_pregunta_3').val();
			if(pregunta == "Selecciona una pregunta"){
				$("#smp").remove();
				$("#select_pregunta_3").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Seleccione una pregunta</small></p>");
				selectInvalido("select_pregunta_3");
				return false;
			}
			else{
					selectValido("select_pregunta_3");
					return true;
				}
		break;
		case "form_pregunta_4":
			var pregunta = $('#select_pregunta_4').val();
			if(pregunta == "Selecciona una pregunta"){
				$("#smp").remove();
				$("#select_pregunta_4").parent().append("<p id='smp' style='color:red; font-style : normal;'><small>Seleccione una pregunta</small></p>");
				selectInvalido("select_pregunta_4");
				return false;
			}
			else{
					selectValido("select_pregunta_4");
					return true;
				}
		break;
	}
}
$(document).ready(function (){ // validación prefijo de cedula
	$('#prefijo_ced').change(function (){
		prefijo = $('#prefijo_ced').val();
		if (prefijo != ""){selectValido("prefijo_ced");}else{selectInvalido("prefijo_ced");}
	});
});
$(document).ready(function (){ // validación de cedula
	$('#form_cedula').keyup(function (){
		cedula = $('#form_cedula').val();
		this.value = (this.value + '').replace(/[^0-9]/, '');
		var expr = /^[0-9]{5,9}$/;
		if (expr.test(cedula) == true){campoValido("form_cedula");}else{campoInvalido("form_cedula");}
	});
});
$(document).ready(function (){ // validación de cedula olvido_correo
	$('#form_cedula_oc').keyup(function (){
		cedula = $('#form_cedula_oc').val();
		this.value = (this.value + '').replace(/[^0-9]/, '');
		var expr = /^[0-9]{5,9}$/;
		if (expr.test(cedula) == true){campoValido("form_cedula_oc");}else{campoInvalido("form_cedula_oc");}
	});
});
// $(document).ready(function (){ // verifica si la cedula ya existe en el ldap
// 	$('#form_cedula').blur(function (){
// 		cedula = $('#form_cedula').val();
// 		if(cedula != ""){
// 				var datos =  { "opcion" : "cedula",	"prefijo" : $('#prefijo_ced').val(), "cedula" : cedula };
// 			$.ajax({
// 			data: datos,
// 			url: '../core/Controllers/verificarDatosController.php',
// 				type: 'post',
// 			success: function(response){
// 						h = response;
// 						if(response == "existe"){
// 							$("#smp").remove();
// 							$('#form_cedula').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>La cédula ya se encuentra registrada</small></p>");
// 							campoInvalido("form_cedula");
// 						}
// 					}
// 				}).always(function(){
// 					console.log(h);
// 				});
// 		}else{

// 			campoInvalido("form_cedula");
// 			$("#smp").remove();
// 		}
// 	});
// });
$(document).ready(function (){ // validación prefijo de rif
	$('#prefijo_rif').change(function (){
		prefijo = $('#prefijo_rif').val();
		if (prefijo != ""){selectValido("prefijo_rif");}else{selectInvalido("prefijo_rif");}
	});
});
// $(document).ready(function (){ // validación de rif
// 	$('#form_rif').keyup(function (){
// 		rif = $('#form_rif').val();
// 		this.value = (this.value + '').replace(/[^0-9]/, '');
// 		var expr = /^[0-9]{6,9}$/;
// 		if (expr.test(rif) == true){campoValido("form_rif");}else{campoInvalido("form_rif");}
// 	});
// });
// $(document).ready(function (){ // validación de rif  olvido correo
// 	$('#form_rif_oc').keyup(function (){
// 		rif = $('#form_rif_oc').val();
// 		this.value = (this.value + '').replace(/[^0-9]/, '');
// 		var expr = /^[0-9]{6,9}$/;
// 		if (expr.test(rif) == true){campoValido("form_rif_oc");}else{campoInvalido("form_rif_oc");}
// 	});
// });
// $(document).ready(function (){ // verifica si el rif ya existe en el ldap
// 	$('#form_rif').blur(function (){
// 		rif = $('#form_rif').val();
// 		if(rif!=""){
// 			var datos =  { "opcion" : "rif",	"prefijo" : $('#prefijo_rif').val(), "rif" : rif };
// 			$.ajax({
// 			data: datos,
// 			url: '../core/Controllers/verificarDatosController.php',
// 				type: 'post',
// 			success: function(response){
// 						if(response == "existe"){
// 							$("#smp").remove();
// 							$('#form_rif').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El RIF ya se encuentra registrado</small></p>");
// 							campoInvalido("form_rif");
// 						}
// 					}
// 				});
// 		}else{

// 			campoInvalido("form_rif");
// 			$("#smp").remove();
// 		}
// 	});
// });
$(document).ready(function (){ // validacion letras form_nombres
	$('#form_nombres').keyup(function (){
	this.value = (this.value + '').replace(/[^a-zA-ZñÑáéíóúÁÉÍÓÚ ]/g, '');
	nombre = $('#form_nombres').val();
	var expr = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ]{2,}/g;
	if (expr.test(nombre) == false){campoInvalido("form_nombres");}else{campoValido("form_nombres");}
	});
});
$(document).ready(function (){ // validacion razon_social
	$('#form_razon_social').keyup(function (){
	this.value = (this.value + '').replace(/[^a-zA-ZñÑáéíóúÁÉÍÓÚ0-9 ]/g, '');
	nombre = $('#form_razon_social').val();
	var expr = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9 ]{3,}/g;
	if (expr.test(nombre) == false){campoInvalido("form_razon_social");}else{campoValido("form_razon_social");}
	});
});
$(document).ready(function (){ // validacion form_apellidos
	$('#form_apellidos').keyup(function (){
	this.value = (this.value + '').replace(/[^a-zA-ZñÑáéíóúÁÉÍÓÚ ]/g, '');
	apellidos = $('#form_apellidos').val();
	var expr = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ]{2,}/g;
	if (expr.test(apellidos) == false){campoInvalido("form_apellidos");}else{campoValido("form_apellidos");}
	});
});
$(document).ready(function (){ // validacion de prefijos de telf contacto
	$('#prefijo_tlf_contacto').change(function (){
		var prefijo_contacto = $("#prefijo_tlf_contacto").val();
		if (prefijo_contacto != ""){selectValido("prefijo_tlf_contacto");}else{selectInvalido("prefijo_tlf_contacto");}
	});
});
$(document).ready(function (){ // validacion tlf_contacto
	$('#form_tlf_contacto').keyup(function (){
		this.value = (this.value + '').replace(/[^0-9\-]/g, '');
		var contacto = $("#form_tlf_contacto").val();
		var expr = /^[0-9]{7}$/;
		if (expr.test(contacto) != false){campoValido("form_tlf_contacto");}else{campoInvalido("form_tlf_contacto");}
	});
});
$(document).ready(function (){ // validacion de prefijos de telf movil
	$('#prefijo_tlf_movil').change(function (){

		var prefijo_movil = $("#prefijo_tlf_movil").val();
		if (prefijo_movil != ""){selectValido("prefijo_tlf_movil");}else{selectInvalido("prefijo_tlf_movil");}
	});
});
$(document).ready(function (){ // validacion tlf_movil
	$('#form_tlf_movil').keyup(function (){
		this.value = (this.value + '').replace(/[^0-9\-]/g, '');
		var movil = $("#form_tlf_movil").val();
		var expr = /^[0-9]{7}$/;
		if (expr.test(movil) != false){campoValido("form_tlf_movil");}else{campoInvalido("form_tlf_movil");}
	});
});
$(document).ready(function (){ // validacion de correo electronico
	$('#form_correo_electronico').keyup(function (){

	correo = $('#form_correo_electronico').val();
	var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
	if (expr.test(correo) == false){campoInvalido("form_correo_electronico");}else{campoValido("form_correo_electronico");}
	});
});
$(document).ready(function (){ // validacion de correo electronico_r
	$('#form_correo_electronico_r').keyup(function (){

	correo = $('#form_correo_electronico_r').val();
	var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
	if (expr.test(correo) == false){campoInvalido("form_correo_electronico_r");}else{campoValido("form_correo_electronico_r");}
	});
});
$(document).ready(function (){ // validacion de correo electronico_r
	$('#form_correo_electronico_m').keyup(function (){

	correo = $('#form_correo_electronico_m').val();
	var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
	if (expr.test(correo) == false){campoInvalido("form_correo_electronico_m");}else{campoValido("form_correo_electronico_m");}
	});
});
$(document).ready(function (){ // verifica si el correo ya existe en el ldap
	$('#form_correo_electronico').blur(function (){
		correo = $('#form_correo_electronico').val();
		if(correo!=""){
			var datos =  { "opcion" : "correo", "correo" : correo };
			$.ajax({
			data: datos,
			url: '../core/Controllers/verificarDatosController.php',
				type: 'post',
        		success: function(response){
				//$('#form_correo_electronico').parent().append(response);
				if(response == "existe"){
					$("#smp").remove();
					$('#form_correo_electronico').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El correo electrónico ingresado ya se encuentra registrado</small></p>");
					campoInvalido("form_correo_electronico");
				}
			}
			});
		}
	});
});
$(document).ready(function (){ // validacion de correo empresa
	$('#form_correo_empresa').keyup(function (){

	correo = $('#form_correo_empresa').val();
	var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
	if (expr.test(correo) == false){campoInvalido("form_correo_empresa");}else{campoValido("form_correo_empresa");}
	});
});
$(document).ready(function (){ // verifica si el correo_empresarial ya existe en el ldap
	$('#form_correo_empresa').blur(function (){
		correo = $('#form_correo_empresa').val();
		if(correo!=""){
		var datos =  { "opcion" : "correo_empresa", "correo" : correo };
			$.ajax({
			data: datos,
			url: '../core/Controllers/verificarDatosController.php',
				type: 'post',
			success: function(response){
						//$('#form_correo_electronico').parent().append(response);
						if(response == "existe"){
							$("#smp").remove();
							$('#form_correo_empresa').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El correo electrónico ingresado ya se encuentra registrado </small></p>");
							campoInvalido("form_correo_empresa");
							return false;
						}
					}
				});
		}
	});
});
$(document).ready(function (){ // validacion de correo empresarial
	$('#form_correo_empresarial').keyup(function (){

	correo = $('#form_correo_empresarial').val();
	var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
	if (expr.test(correo) == false){campoInvalido("form_correo_empresarial");}else{campoValido("form_correo_empresarial");}
	});
});
$(document).ready(function (){ // verifica si el correo_empresarial ya existe en el ldap
	$('#form_correo_empresarial').blur(function (){
		correo = $('#form_correo_empresarial').val();
		if(correo!=""){
		var datos =  { "opcion" : "correo", "correo" : correo };
			$.ajax({
			data: datos,
			url: '../core/Controllers/verificarDatosController.php',
				type: 'post',
			success: function(response){
						//$('#form_correo_electronico').parent().append(response);
						if(response == "existe"){
							$("#smp").remove();
							$('#form_correo_empresarial').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El correo electrónico ingresado ya se encuentra registrado </small></p>");
							campoInvalido("form_correo_empresarial");
						}
					}
				});
		}
	});
});
$(document).ready(function (){ // validacion de nuevo correo
	$('#form_nuevo_correo').keyup(function (){
	correo = $('#form_nuevo_correo').val();
	var expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
	if (expr.test(correo) == false){campoInvalido("form_nuevo_correo");}else{campoValido("form_nuevo_correo");}
	});
});
/*$(document).ready(function (){ // validacion representante de venta
	$('#form_representante_venta').keyup(function (){
	this.value = (this.value + '').replace(/[^a-zA-ZñÑáéíóúÁÉÍÓÚ ]/g, '');
	nombre = $('#form_representante_venta').val();
	var expr = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ]{2,}/g;
	if (expr.test(nombre) == false){campoInvalido("form_representante_venta");}else{campoValido("form_representante_venta");}
	});
});*/
$(document).ready(function (){ // validacion de contraseña
	$('#form_contrasena').keyup(function (){
		contrasena = $('#form_contrasena').val();
		var expr = /^(?!.*(.)\1)[A-Z]+(?=.*\d)(?=.*[¿:;!\\\{\}\[\]#*$”?\=!\)\(/°%&\.,+_\-])(?=.*[a-z])\S{7,25}$/;
		if (expr.test(contrasena) == false){campoInvalido("form_contrasena");}else{campoValido("form_contrasena");}
	});
});
$(document).ready(function (){ // validacion de confirmación de contraseña
	$('#form_confirm_contrasena').keyup(function (){
		contrasena = $('#form_contrasena').val();
		contrasena2 = $('#form_confirm_contrasena').val();

		if (contrasena == contrasena2){campoValido("form_confirm_contrasena");}else{campoInvalido("form_confirm_contrasena");}
	});
});
$(document).ready(function (){ // validacion de contraseña
	$('#form_contrasena_actual').keyup(function (){
		contrasena = $('#form_contrasena_actual').val();
		var expr = /^(?!.*(.)\1)[A-Z]+(?=.*\d)(?=.*[¿:;!\\\{\}\[\]#*$”?\=!\)\(/°%&\.,+_\-])(?=.*[a-z])\S{7,25}$/;
		if (expr.test(contrasena) == false){campoInvalido("form_contrasena_actual");}else{campoValido("form_contrasena_actual");}
	});
});
$(document).ready(function (){ // validacion de correo empresarial
	$('#form_correo_empresarial').keyup(function (){
		correo = $('#form_correo_empresa').val();
		correo2 = $('#form_correo_empresarial').val();
		if (correo == correo2){
			campoInvalido("form_correo_empresarial");}

		else{
			campoValido("form_correo_empresarial");
		}
	});
});
$(document).ready(function (){ // validacion usuario
	$('#form_usuario').keyup(function (){
	this.value = (this.value + '').replace(/[^a-zA-Z0-9\-\_\.]{8,}/g, '');
	nombre = $('#form_usuario').val();
	var expr = /^[a-zA-Z0-9\-\_\.]{8,}/g;
	if (expr.test(nombre) == false){campoInvalido("form_usuario");}else{campoValido("form_usuario");}
	});
});
$(document).ready(function (){ // verifica si el usuario ya existe en el ldap
	$('#form_usuario').blur(function (){
		usuario = $('#form_usuario').val();
		if(usuario!=""){
			var datos =  { "opcion" : "usuario", "usuario" : usuario };
			$.ajax({
			data: datos,
			url: '../core/Controllers/verificarDatosController.php',
				type: 'post',
			success: function(response){
						if(response == "existe"){
							$("#smp").remove();
							$('#form_usuario').parent().append("<p id='smp' style='color:red; font-style : italic;'><small>El usuario ingresado ya existe</small></p>");
							campoInvalido("form_usuario");
						}
					}
				});
		}
	});
});
$(document).ready(function (){ // validación pregunta 1
	$('#select_pregunta_1').change(function (){
		prefijo = $('#prefijo_rif').val();
		if (prefijo != ""){selectValido("select_pregunta_1");}else{selectInvalido("select_pregunta_1");}
	});
});
$(document).ready(function (){ // respuesta 1
	$('#form_respuesta_1').keyup(function (){
		this.value = (this.value + '').replace(/[^a-zA-ZñÑáéíóúÁÉÍÓÚ0-9 ]/g, '');
		r1 = $('#form_respuesta_1').val();
		var expr = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ]{2,}/g;
		if (expr.test(r1) == false){campoInvalido("form_respuesta_1");}else{campoValido("form_respuesta_1");}
	});
});
$(document).ready(function (){ // respuesta 2
	$('#form_respuesta_2').keyup(function (){
		this.value = (this.value + '').replace(/[^a-zA-ZñÑáéíóúÁÉÍÓÚ0-9 ]/g, '');
		r1 = $('#form_respuesta_1').val();
		var expr = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ]{2,}/g;
		if (expr.test(r2) == false){campoInvalido("form_respuesta_2");}else{campoValido("form_respuesta_2");}
	});
});
$(document).ready(function (){ // respuesta 3
	$('#form_respuesta_3').keyup(function (){
		this.value = (this.value + '').replace(/[^a-zA-ZñÑáéíóúÁÉÍÓÚ0-9 ]/g, '');
		r3 = $('#form_respuesta_3').val();
		var expr = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ]{2,}/g;
		if (expr.test(r3) == false){campoInvalido("form_respuesta_3");}else{campoValido("form_respuesta_3");}
	});
});
$(document).ready(function (){ // respuesta 4
	$('#form_respuesta_4').keyup(function (){
		this.value = (this.value + '').replace(/[^a-zA-ZñÑáéíóúÁÉÍÓÚ0-9 ]/g, '');
		r4 = $('#form_respuesta_4').val();
		var expr = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ]{2,}/g;
		if (expr.test(r4) == false){campoInvalido("form_respuesta_4");}else{campoValido("form_respuesta_4");}
	});
});
$(document).ready(function (){ // identifica y verifica la imagen que se seleccionó
	$('.containerx div').click(function(){
		$('.containerx div').removeClass('borde-rojo');
		nombre_div = ($(this).attr('id'));
		$("#"+nombre_div).addClass('borde-rojo');
		$("#"+nombre_div+" input").prop("checked", true);
  })
})
$(document).ready(function (){ // verifica que se acepten las politicas de seguridad
	$('#checkbox').click(function(){
	if($("#checkbox").is(':checked')) {
            $("#boton-registrar").removeAttr('disabled');
        }
	else {
           $("#boton-registrar").prop('disabled', true);
       }
	})
})

