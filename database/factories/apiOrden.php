<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\apiOrden;
use Faker\Generator as Faker;

$factory->define(apiOrden::class, function (Faker $faker) {
    return [
        //
        'orden' => $faker->sentence,
        'cuenta_contrato' =>  $faker->unique()->sentence,
        'rif_O_Cedula' => $faker->unique()->sentence,
        'serial' =>  $faker->unique()->sentence,
        'Dispositivo' =>  '239-1234567',
        'creado' =>  $faker->sentence,
        'asignado' =>  $faker->sentence,
        'cola' =>  $faker->sentence,
        'estado' =>  $faker->sentence,
        'tipo' =>  $faker->sentence,
        'created_at' => '2021-06-19 15:38:32',
        'updated_at' => '2021-06-19 15:38:32',
    ];
});
