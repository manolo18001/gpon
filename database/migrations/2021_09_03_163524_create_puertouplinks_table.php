<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePuertouplinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puertouplinks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_olt');
            $table->integer('id_uplink');
            $table->integer('cant_uplink');
            $table->boolean('estado');
            $table->timestamps();
            $table->foreign('id_olt')->references('id_olt')->on('olts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puertouplinks');
    }
}
