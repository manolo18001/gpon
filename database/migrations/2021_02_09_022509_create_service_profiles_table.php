<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_profiles', function (Blueprint $table) {
            $table->id('id');
            $table->string('name_servicio');
            // $table->boolean('status');
            $table->string('user_email_created');
            $table->string('modelo_ont');
            $table->string('user_email_updated')->nullable();
            $table->integer('ont_id')->unsigned();
            $table->foreign('ont_id')->references('id')->on('onts')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->char('ID_estatus')->unsigned();
            $table->foreign('ID_estatus')->references('Estatus')->on('statuses')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_profiles');
    }
}
