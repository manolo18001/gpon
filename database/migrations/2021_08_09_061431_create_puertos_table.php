<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePuertosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puertos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('puerto', 25);
            $table->string('estado', 25);

            $table->integer('id_targetPort')->unsigned();
            $table->foreign('id_targetPort')->references('id')->on('target_ports')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('id_sfp')->unsigned();
            $table->foreign('id_sfp')->references('id')->on('sfps')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->string('user_email_created')->nullable();
            $table->string('user_email_updated')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puertos');
    }
}
