<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_product', 25);
            // $table->integer('perfil_id')->unsigned();
            $table->json('secondlist');
            $table->boolean('status');
            $table->string('user_email_created');
            $table->string('user_email_updated')->nullable();
            // $table->foreign('perfil_id')->references('id')->on('service_profiles')
            // ->onDelete('cascade')
            //     ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
