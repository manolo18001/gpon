<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSplitters2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('splitters2', function (Blueprint $table) {
            $table->increments('id');
            $table->string('previo');
            $table->integer('id_splitter');
            $table->string('splitter2');
            $table->string('nivelDivision');
            $table->string('capacidad');
            $table->string('perdida');
            $table->string('latitud');
            $table->string('longitud');
            $table->timestamps();
            $table->foreign('id_splitter')->references('id')->on('splitters')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('splitters2');
    }
}
