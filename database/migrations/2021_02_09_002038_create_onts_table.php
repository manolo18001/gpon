<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOntsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('onts', function (Blueprint $table) {
            // $table->id();
            $table->increments('id');

            // $table->string('proveedor', 25);
            $table->string('serial', 12);
            $table->string('modelo',25);
            // $table->boolean('status');
            $table->string('user_email_created');
            $table->string('user_email_updated')->nullable();

            $table->integer('tipo')->unsigned();
            $table->foreign('tipo')->references('id')->on('ont_tipos')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->char('ID_estatus')->unsigned();
            $table->foreign('ID_estatus')->references('Estatus')->on('statuses')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('proveedor')->unsigned();
            $table->foreign('proveedor')->references('id')->on('proveedors')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('onts');
    }
}
