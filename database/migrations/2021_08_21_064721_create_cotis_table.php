<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCotisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotis', function (Blueprint $table) {
            // $table->id();
            $table->increments('id');
            $table->string('aptoCas', 25);
            $table->string('nombre', 25);
            $table->integer('puertp')->nullable();
            $table->boolean('acometida')->nullable();
            $table->string('user_email_created')->nullable();
            $table->string('user_email_updated')->nullable();
            $table->integer('id_install');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotis');
    }
}
