<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAptonombresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aptonombres', function (Blueprint $table) {
            $table->increments('id');
            $table->string('aptoCas', 25);
            $table->string('nombre', 25);
            $table->string('user_email_created')->nullable();
            $table->string('user_email_updated')->nullable();
            $table->integer('id_install')->unsigned();
            $table->foreign('id_install')->references('id')->on('installs')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aptonombres');
    }
}
