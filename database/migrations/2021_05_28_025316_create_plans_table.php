<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            /* sin conceptualizacion */
            $table->string('Nombre_plan', 50);
            $table->integer('Stream_down');// de cuatro digitos
            $table->integer('Stream_up');// de cuatro digitos
            $table->integer('ID_inboundtraffic')->unsigned();// de dos digitos
            $table->integer('ID_outboundtraffic')->unsigned();// de dos digitos
            $table->date('Fecha_inicio')->nullable();
            // $table->date('Fecha_fin')->nullable();
            // $table->integer('ID_estatus')->unsigned();
            // $table->foreign('ID_estatus')->references('id')->on('statuses')
            // ->onDelete('cascade')
            // ->onUpdate('cascade');
            // $table->integer('perfil_id')->unsigned();
            $table->boolean('status');
            // $table->string('user_email_created');
            // $table->string('user_email_updated')->nullable();
            // $table->foreign('perfil_id')->references('id')->on('online_profiles')
            //     ->onDelete('cascade')
            //     ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
