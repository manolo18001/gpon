<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOntUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ont_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo', 25);

            // $table->integer('ID_TIPOS_ONT')->unsigned();
            // $table->foreign('ID_TIPOS_ONT')->references('id')->on('ont_tipos')
            // ->onDelete('cascade')
            // ->onUpdate('cascade');
            // // $table->integer('SVLAN')->unsigned();
            // // $table->foreign('SVLAN')->references('id')->on('servicio')
            // // ->onDelete('cascade')
            // // ->onUpdate('cascade');
            // // $table->integer('id_plan')->unsigned();
            // // $table->foreign('id_plan')->references('id')->on('plans')
            // // ->onDelete('cascade')
            // // ->onUpdate('cascade');
            // $table->integer('olt_id')->unsigned();
            // $table->foreign('olt_id')->references('id')->on('olts')
            //     ->onDelete('cascade')
            //     ->onUpdate('cascade');
            // $table->integer('SVLAN'); // de cuatro digitos
            // $table->integer('CVLAN'); // de cuatro digitos
            // $table->integer('ID_estatus')->unsigned();
            // $table->foreign('ID_estatus')->references('id')->on('statuses')
            // ->onDelete('cascade')
            // ->onUpdate('cascade');
            // $table->integer('Stream_down'); // de cuatro digitos
            // $table->integer('Stream_up');// de cuatro digitos




            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ont_usuarios');
    }
}
