<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMangas2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mangas2', function (Blueprint $table) {
            $table->increments('id');
            $table->string('previo');
            $table->integer('id_mangas');
            $table->string('manga2');
            $table->string('perdida');
            $table->string('tipo_cable');
            $table->string('cant_saliente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mangas2');
    }
}
