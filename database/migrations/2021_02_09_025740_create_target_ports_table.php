<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTargetPortsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_ports', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('ip_agregador', 15);
            $table->integer('ranura_tarjeta');
            $table->string('nombre_tarjeta', 25);
            $table->string('tipo_target', 25);
            $table->integer('VPI');
            $table->integer('S_VLAN');
            $table->integer('puerto_inicial');
            $table->integer('cant_puertos');
            $table->integer('puertos');// puerto_inicial por cant_puertos
            $table->integer('cant_ont');
            $table->integer('C_lan_ini');
            $table->integer('C_lan'); // C_lan_ini por cant_ont
            // $table->boolean('status');
            $table->char('ID_estatus')->unsigned();
            $table->integer('olt_id')->unsigned();
            $table->string('user_email_created');
            $table->string('user_email_updated')->nullable();
            $table->foreign('ID_estatus')->references('Estatus')->on('statuses')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('olt_id')->references('id')->on('olts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_ports');
    }
}
