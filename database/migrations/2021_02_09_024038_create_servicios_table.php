<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->char('VLAN', 2)->unique();
            $table->text('Servicio');//de 50 digitos
            $table->text('Regla')->nullable();
            $table->integer('GEMPORT')->nullable(); // de cuatro digitos
            $table->char('ID_estatus')->unsigned();
            $table->foreign('ID_estatus')->references('Estatus')->on('statuses')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios');
    }
}
