<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnlineProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_profiles', function (Blueprint $table) {
            $table->id('id');
            $table->string('name_online');
            $table->integer('Stream_down'); // de cuatro digitos
            $table->integer('Stream_up');// de cuatro digitos
            // $table->boolean('status');
            $table->integer('tipo_producto')->unsigned();
            $table->foreign('tipo_producto')->references('id')->on('products')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('ont_usuarios_id')->unsigned();
            $table->foreign('ont_usuarios_id')->references('id')->on('ont_tipo_srvs')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->char('ID_estatus')->unsigned();
            $table->foreign('ID_estatus')->references('Estatus')->on('statuses')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->string('user_email_created');
            $table->string('user_email_updated')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_profiles');
    }
}
