<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centrals', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('ip_agregador', 15);
            $table->string('codigo_central', 5);
            $table->string('name_central', 25);
            $table->boolean('status');
            $table->integer('zona_p_central');
            $table->string('direcc', 25);
            $table->string('user_email_created');
            $table->string('user_email_updated')->nullable();
            $table->string('urbanizacion',25);
            $table->integer('estado_id')->unsigned();
            $table->foreign('estado_id')->references('id')->on('estados')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('ciudad_id')->unsigned();
            $table->foreign('ciudad_id')->references('id')->on('ciudads')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->integer('municipio_id')->unsigned();
            $table->foreign('municipio_id')->references('id')->on('municipios')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('parroquia_id')->unsigned();
            $table->foreign('parroquia_id')->references('id')->on('parroquias')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            // $table->integer('urbanizacion_id')->unsigned();
            // $table->foreign('urbanizacion_id')->references('id')->on('urbanizacions')
            // ->onDelete('cascade')
            //     ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centrals');
    }
}
