<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOltsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fila');
            $table->integer('piso');
            $table->integer('sala');
            $table->integer('bastidor'); // de dos digitos
            $table->integer('subbastidor'); // de dos digitos
            $table->ipAddress('ip_olt')->unsigned();
            $table->string('Nombre_elemento', 25);
            $table->string('Tipo', 25); //modelo
            $table->integer('puerto_serv_inicial');
            $table->integer('id_proveedor');
            $table->integer('puerto_serv_Xnumero_puertos');
            $table->integer('numero_puertos');
            $table->integer('puerto_uplin');
            $table->integer('central_id')->unsigned();

            $table->string('user_email_created');
            $table->string('user_email_updated')->nullable();
            $table->foreign('central_id')->references('id')->on('centrals')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('id_proveedor')->references('id')->on('proveedors')
             ->onDelete('cascade')
             ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olts');
    }
}
