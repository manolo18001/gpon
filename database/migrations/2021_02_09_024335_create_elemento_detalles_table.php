<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElementoDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elemento_detalles', function (Blueprint $table) {
            $table->increments('id');
            $table->char('ID_elementod', 10)->unique();
            // $table->char('ID_elemento', 10)->unique();
            $table->integer('Slot')->nullable(); // de dos digitos
            $table->integer('Puerto')->nullable(); // de tres digitos
            $table->string('ID_puerto', 40);
            $table->integer('SVLAN'); // de cuatro digitos
            $table->integer('CVLAN')->nullable(); // de cuatro digitos
            $table->char('ID_elemento_post', 10)->nullable();
            $table->char('ID_estatus')->unsigned();
            $table->char('ID_elemento')->unsigned();
            $table->foreign('ID_elemento')->references('ID_elementod')->on('elemento_reds')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('ID_estatus')->references('Estatus')->on('statuses')
            ->onDelete('cascade')
            ->onUpdate('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elemento_detalles');
    }
}
