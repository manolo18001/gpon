<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSfpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sfps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo');
            $table->char('nameSfp',10);
            $table->integer('Stream_up');
            $table->integer('Stream_down');
            $table->string('user_email_created')->nullable();
            $table->string('user_email_updated')->nullable();
            $table->timestamps();
            $table->foreign('id_tipo')->references('id')->on('tipos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sfps');
    }
}
