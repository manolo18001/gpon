<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiOrdensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_ordens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('orden',13);
            $table->string('cuenta_contrato', 12);
            $table->char('Dispositivo',11);
            $table->string('rif_O_Cedula', 9);
            $table->string('serial', 10);
            $table->string('creado');
            $table->string('asignado');
            $table->string('cola', 25);
            $table->string('estado', 25);
            $table->string('tipo', 25);
            $table->string('nombre_cliente');
            $table->string('direccion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_ordens');
    }
}
