<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMangasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mangas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('previo');
            $table->integer('id_odf');
            $table->string('manga',25);
            $table->string('perdida');
            $table->string('cable',25);
            $table->string('cantidad',25);
            $table->string('user_email_created')->nullable();
            $table->string('user_email_updated')->nullable();
            $table->timestamps();
            $table->foreign('id_odf')->references('id')->on('odfs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mangas');
    }
}
