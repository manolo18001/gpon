<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSplittersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('splitters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('previo');
            $table->integer('id_manga2')->nullable();
            $table->string('splitter1');
            $table->string('nivelDivision');
            $table->string('capacidad');
            $table->string('perdida');
            $table->string('latitud');
            $table->string('longitud');
            $table->timestamps();
            $table->foreign('id_manga')->references('id')->on('mangas')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('id_manga2')->references('id')->on('mangas2')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('splitters');
    }
}
