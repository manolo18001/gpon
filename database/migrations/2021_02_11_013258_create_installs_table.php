<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('installs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rack_olt', 25);
            $table->integer('olt');
            $table->integer('frame_p_olt');
            $table->integer('slot_olt');
            $table->integer('puerto_olt');
            $table->integer('rack_odfin')->nullable();
            $table->integer('odfin')->nullable();
            $table->integer('bandeja_odfin')->nullable();
            $table->integer('pos_odfin')->nullable();
            $table->integer('rack_odfout')->nullable();
            $table->integer('odfout')->nullable();
            $table->integer('bandeja_odfout')->nullable();
            $table->integer('pos_odfout')->nullable();
            $table->integer('feeder')->nullable();
            $table->integer('rack_odfpe');
            // $table->integer('rack_odfpe:');
            $table->integer('odfpe');
            $table->integer('bandeja_odfpe');
            $table->integer('pos_odfpe');
            $table->integer('fibra');
            $table->string('hilo', 25);
            $table->string('buffer', 25);
            $table->integer('manga');
            $table->integer('gdp')->nullable();
            $table->integer('mfs')->nullable();
            $table->integer('gds')->nullable();
            $table->string('fxb_term', 25);
            $table->integer('Cantidad_FXB');
            $table->integer('cant_mbx_terminal');
            $table->string('destino', 25);
            $table->integer('grados_la');
            // $table->integer('min_la');
            // $table->integer('seg_la');
            // $table->integer('seg_dec_la');
            $table->string('dirección_la', 25);
            $table->integer('grados_lo');
            // $table->integer('min_lo');
            // $table->integer('seg_lo');
            // $table->integer('seg_dec_lo');
            $table->string('dirección_lo', 25);
            $table->string('urbanizacion', 25);
            $table->integer('central_id')->unsigned();

            $table->foreign('central_id')->references('id')->on('centrals')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('installs');
    }
}
