<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElementoRedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elemento_reds', function (Blueprint $table) {
            $table->increments('id');
            $table->char('ID_elementod',10)->unique();// de diez digitos
            $table->string('Nombre_elemento', 25);
            $table->integer('Slot')->nullable(); // de dos digitos
            $table->integer('Puertos')->nullable(); // de TRES digitos
            $table->integer('id_proveedor')->unsigned();
            // $table->string('Tipo', 20);
            $table->char('id_tipo')->unsigned();
            $table->char('Despliegue', 1);
            $table->char('ID_estatus')->unsigned();
            $table->foreign('ID_estatus')->references('Estatus')->on('statuses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('id_proveedor')->references('id')->on('proveedors')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('id_tipo')->references('Tipo')->on('tipos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elemento_reds');
    }
}
