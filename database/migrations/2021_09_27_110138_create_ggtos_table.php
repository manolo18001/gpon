<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGgtosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ggtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_splitter')->nullable();
            $table->integer('id_splitter2')->nullable();
            $table->boolean('drop');
            $table->string('distancia');
            $table->string('ruta');
            $table->string('elementoSaliente');
            $table->string('user_email_created')->nullable();
            $table->string('user_email_updated')->nullable();
            $table->integer('id_install');
            $table->timestamps();
            $table->foreign('id_splitter')->references('id')->on('splitters')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('id_splitters2')->references('id')->on('splitters2')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ggtos');
    }
}
