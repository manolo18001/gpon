<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOdfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('odfs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('odf');
            $table->string('sala');
            $table->string('piso');
            $table->string('bastidor');
            $table->string('frame');
            $table->string('bandeja');
            $table->string('posicion');
            $table->string('tipo_cable');
            $table->string('capacidad');
            $table->string('distancia');
            $table->string('ruta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('odfs');
    }
}
