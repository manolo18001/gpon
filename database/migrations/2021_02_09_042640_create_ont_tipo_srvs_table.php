<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOntTipoSrvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ont_tipo_srvs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipos_user');
            // $table->integer('ID_TIPOS_ONT')->unsigned();
            // $table->foreign('ID_TIPOS_ONT')->references('id')->on('ont_tipos')
            //     ->onDelete('cascade')
            //     ->onUpdate('cascade');
            // $table->integer('SVLAN')->unsigned();
            // $table->foreign('SVLAN')->references('id')->on('servicios')
            //     ->onDelete('cascade')
            //     ->onUpdate('cascade');
            // $table->integer('id_service_profiles')->unsigned();
            // $table->foreign('id_service_profiles')->references('id')->on('service_profiles')
            //     ->onDelete('cascade')
            //     ->onUpdate('cascade');
            // $table->integer('id_online_profiles')->unsigned();
            // $table->foreign('id_online_profiles')->references('id')->on('online_profiles')
            //     ->onDelete('cascade')
            //     ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ont_tipo_srvs');
    }
}
