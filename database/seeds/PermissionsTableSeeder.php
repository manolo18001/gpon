<?php

// use Caffeinated\Shinobi\Contracts\Permission;
use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $permission = new Permission();
        // $permission->name = 'Navegar usuarios';
        // $permission->slug = 'user.index';
        // $permission->description = 'lista y navega todos los usuarios del sistema';
        // $permission->save();
        // $permission = new Permission();
        // $permission->name = 'Ver detalle de usuarios';
        // $permission->slug = 'users.show';
        // $permission->description = 'Ver detalle de cada usuario del sistema';
        // $permission->save();
        //USERS
        Permission::insert([
            'name'          => 'Navegar usuarios',
            'slug'          => 'users.index',
            'description'   => 'lista y navega todos los usuarios del sistema'
        ]);
        Permission::insert([
            'name'  => 'Ver detalle de usuarios',
            'slug'  => 'users.show',
            'description'  => 'Ver detalle de cada usuario del sistema'
        ]);
        Permission::insert([
            'name'  => 'Edicion de usuarios',
            'slug'  => 'users.edit',
            'description'  => 'Editar cualquier dato de un usuario del sistema',
        ]);
        Permission::insert([
            'name'  => 'Eliminar usuario',
            'slug'  => 'users.destroy',
            'description'  => 'Eliminar cualquier dato de un usuario del sistema',
        ]);
        Permission::insert([
            'name'  => 'Crear usuario',
            'slug'  => 'users.insert',
            'description'  => 'Crear usuario del sistema',
        ]);

        //Roles
        Permission::insert([
            'name'  => 'Navegar Roles',
            'slug'  => 'roles.index',
            'description'  => 'lista y navega todos los roles del sistema',
        ]);
        Permission::insert([
            'name'  => 'Ver detalle de roles',
            'slug'  => 'roles.show',
            'description'  => 'Ver detalle de cada rol del sistema',
        ]);
        Permission::insert([
            'name'  => 'Edicion de roles',
            'slug'  => 'roles.edit',
            'description'  => 'Editar cualquier dato de un rol del sistema',
        ]);
        Permission::insert([
            'name'  => 'Eliminar rol',
            'slug'  => 'roles.destroy',
            'description'  => 'Eliminar cualquier dato de un rol del sistema',
        ]);
        Permission::insert([
            'name'  => 'Crear rol',
            'slug'  => 'roles.insert',
            'description'  => 'Crear rol del sistema',
        ]);

        //centrales
        Permission::insert([
            'name'  => 'Navegar centrales',
            'slug'  => 'central.index',
            'description'  => 'lista y navega todos los centrales del sistema',
        ]);
        Permission::insert([
            'name'  => 'Ver detalle de centrales',
            'slug'  => 'central.show',
            'description'  => 'Ver detalle de cada central del sistema',
        ]);
        Permission::insert([
            'name'  => 'Edicion de centrales',
            'slug'  => 'central.edit',
            'description'  => 'Editar cualquier dato de un central del sistema',
        ]);
        Permission::insert([
            'name'  => 'Eliminar central',
            'slug'  => 'central.destroy',
            'description'  => 'Eliminar cualquier dato de un central del sistema',
        ]);
        Permission::insert([
            'name'  => 'Crear central',
            'slug'  => 'central.insert',
            'description'  => 'Crear central del sistema',
        ]);
        //configuraciones
        Permission::insert([
            'name'  => 'Navegar configuraciones',
            'slug'  => 'configura.index',
            'description'  => 'lista y navega todos los configuraciones del sistema',
        ]);
        Permission::insert([
            'name'  => 'Ver detalle de configuraciones',
            'slug'  => 'configura.show',
            'description'  => 'Ver detalle de cada configuración del sistema',
        ]);
        Permission::insert([
            'name'  => 'Edicion de configuraciones',
            'slug'  => 'configura.edit',
            'description'  => 'Editar cualquier dato de un configuración del sistema',
        ]);
        Permission::insert([
            'name'  => 'Eliminar configuración',
            'slug'  => 'configura.destroy',
            'description'  => 'Eliminar cualquier dato de un configuración del sistema',
        ]);
        Permission::insert([
            'name'  => 'Crear configuración',
            'slug'  => 'configura.insert',
            'description'  => 'Crear configuración del sistema',
        ]);


    }
}
