<?php

use Illuminate\Database\Seeder;
use App\estado;

class estadoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $estado = new estado();
        $estado->estado = 'DISTRITO CAPITAL';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'MIRANDA';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'VARGAS';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'ARAGUA';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'CARABOBO';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'LARA';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'YARACUY';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'PORTUGUESA';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'GUARICO';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'COJEDES';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'ANZOATEGUI';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'NUEVA ESPARTA';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'AMAZONAS';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'ZULIA';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'TRUJILLO';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'TACHIRA';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'SUCRE';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'MONAGAS';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'MERIDA';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'FALCON';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'DELTA AMACURO';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'APURE';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'BARINAS';
        $estado->save();
        $estado = new estado();
        $estado->estado = 'BOLIVAR';
        $estado->save();


    }
}
