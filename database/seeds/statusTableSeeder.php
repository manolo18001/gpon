<?php

use Illuminate\Database\Seeder;
use App\status;
use Illuminate\Support\Facades\DB;

class statusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        status::create([
            'Estatus' => 'ACT',
            'descripcion' => 'Activo',
            'acti' => true,
        ]);
        status::create([
            'Estatus' => 'DES',
            'descripcion' => 'No activo',
            'acti' => true,
        ]);
        status::create([
            'Estatus' => 'RES',
            'descripcion' => 'Reservado',
            'acti' => false,
        ]);
        status::create([
            'Estatus' => 'DED',
            'descripcion' => 'Dedicado',
            'acti' => false,
        ]);

        return DB::insert('INSERT INTO role_user (role_id, user_id) values (1, 1),(1,9),(1,10),(1,11);');
    }
}
