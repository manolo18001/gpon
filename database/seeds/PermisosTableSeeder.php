<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;
use App\User;
use Illuminate\Support\Facades\Hash;
// use Caffeinated\Shinobi\Models\Permission_user;
use Illuminate\Support\Facades\DB;

class PermisosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Role::insert([
            'name' => 'Admin',
            'slug'=> 'admin',
            'special' => 'all-access'
        ]);
        User::create([
            'name' => 'mayerling mendoza',
            'email' => 'mmendo07@cantv.com.ve',
            'password' => Hash::make('12345678'),
        ]);
        User::create([
            'name' => 'jorge chaparro',
            'email' => 'jchapa03@cantv.com.ve',
            'password' => Hash::make('12345678'),
        ]);
        User::create([
            'name' => 'luis quintero',
            'email' => 'melend02@labcantv.com.ve',
            'password' => Hash::make('12345678'),
        ]);
        User::create([
            'name' => 'juan ramirez',
            'email' => 'jorge@cantv.com.ve',
            'password' => Hash::make('12345678'),
        ]);
        User::create([
            'name' => 'josu cumana',
            'email' => 'jorge@ifofj.com',
            'password' => Hash::make('12345678'),
        ]);
        User::create([
            'name' => 'Welner Arnaudez',
            'email' => 'warnau01@cantv.com.ve',
            'password' => Hash::make('12345678'),
        ]);
        User::create([
            'name' => 'Jairo Gonzales',
            'email' => 'jgonza141@cantv.com.ve',
            'password' => Hash::make('12345678'),
        ]);
        User::create([
            'name' => 'Gustabo Bolivar',
            'email' => 'gboliv01@cantv.com.ve',
            'password' => Hash::make('12345678'),
        ]);
        User::create([
            'name' => 'Andres Sobcz',
            'email' => 'asobcz01@cantv.com.ve',
            'password' => Hash::make('12345678'),
        ]);
        User::create([
            'name' => 'Daniela Tovar',
            'email' => 'dtovar@cantv.com.ve',
            'password' => Hash::make('12345678'),
        ]);
        User::create([
            'name' => 'Mariela Galindez',
            'email' => 'mgalin@cantv.com.ve',
            'password' => Hash::make('12345678'),
        ]);
        return DB::insert('INSERT INTO permission_user (permission_id, user_id) values (1, 5),(6, 3),(11, 4),(16, 2),(11, 6),(11, 7),(11, 8),(1, 9);');
        // return DB::insert('INSERT INTO permission_user (permission_id, user_id) values (6, 3)');
        // return DB::insert('INSERT INTO permission_user (permission_id, user_id) values (11, 4)');
        // return DB::insert('INSERT INTO permission_user (permission_id, user_id) values (16, 2)');



    }
}
