<?php

use Illuminate\Database\Seeder;
use App\apiOrden;
class apiOrdenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // apiOrden::truncate();
        $faker = \Faker\Factory::create();

        for ($i=0; $i < 50; $i++) {
            # code...


            apiOrden::create([
                'orden' => $faker->numberBetween(12,13),
                'cuenta_contrato' =>  $faker->numberBetween(11,12),
                'Dispositivo' => '239-1234567',
                'rif_O_Cedula' => $faker->numberBetween(8,9),
                'serial' =>  $faker->numberBetween(9,10),
                'creado' =>  $faker->name,
                'asignado' =>  $faker->name,
                'cola' =>     'iiciañ',
                'estado' =>  'abierto',
                'tipo' => 'servicio',
                'nombre_cliente' => $faker->name,
                'direccion' => 'ocumare',
            ]);

            apiOrden::create([
                'orden' => $faker->numerify,
                'cuenta_contrato' =>  $faker->numerify,
                'Dispositivo' => '239-1234567',
                'rif_O_Cedula' => $faker->numerify,
                'serial' =>  $faker->numerify,
                'creado' =>  $faker->email,
                'asignado' =>  $faker->email,
                'cola' =>     'iiciañ',
                'estado' =>  'Proceso',
                'tipo' => 'servicio',
                'nombre_cliente' => $faker->name,
                'direccion' => 'ocumare',
            ]);
            apiOrden::create([
                'orden' => $faker->numerify,
                'cuenta_contrato' =>  $faker->numerify,
                'Dispositivo' => '239-1234567',
                'rif_O_Cedula' => $faker->numerify,
                'serial' =>  $faker->numerify,
                'creado' =>  $faker->name,
                'asignado' =>  $faker->name,
                'cola' =>     'iiciañ',
                'estado' =>  'asignado',
                'tipo' => 'servicio',
                'nombre_cliente' => $faker->name,
                'direccion' => 'caracas',
            ]);
            apiOrden::create([
                'orden' => $faker->numerify,
                'cuenta_contrato' =>  $faker->numerify,
                'Dispositivo' => '239-1234567',
                'rif_O_Cedula' => $faker->numerify,
                'serial' =>  $faker->numerify,
                'creado' =>  $faker->name,
                'asignado' =>  $faker->name,
                'cola' =>     'iiciañ',
                'estado' =>  'cerrado',
                'tipo' => 'servicio',
                'nombre_cliente' => $faker->name,
                'direccion' => 'yare',
            ]);

        }


        // apiOrden::create([
        //     'orden' => '1343243242323',
        //     'cuenta_contrato' => '13432432423',
        //     'rif_O_Cedula' => '134324324',
        //     'serial' => '1343243232',
        //     'creado' => 'jorgecjja',
        //     'asignado' => 'carcas',
        //     'cola' => 'iiciañ',
        //     'estado' => 'abierto',
        //     'tipo' => 'servicio',
        //     // 'created_at' => '"2021-06-19 15:38:32"',
        //     // 'updated_at' => '"2021-06-19 15:38:32"',
        // ]);
        // apiOrden::create([
        //     'orden' => '1343243242323',
        //     'cuenta_contrato' => '13432432423',
        //     'rif_O_Cedula' => '123456789',
        //     'serial' => '1343243232',
        //     'creado' => 'jorgecjja',
        //     'asignado' => 'carcas',
        //     'cola' => 'iiciañ',
        //     'estado' => 'Proceso',
        //     'tipo' => 'servicio',
        //     // 'created_at' => '"2021-06-19 15:38:32"',
        //     // 'updated_at' => '"2021-06-19 15:38:32"',
        // ]);
        // apiOrden::create([
        //     'orden' => '1343243242323',
        //     'cuenta_contrato' => '13432432423',
        //     'rif_O_Cedula' => '12345678',
        //     'serial' => '1343243232',
        //     'creado' => 'jorgecjja',
        //     'asignado' => 'carcas',
        //     'cola' => 'iiciañ',
        //     'estado' => 'asignado',
        //     'tipo' => 'servicio',
        //     // 'created_at' => '"2021-06-19 15:38:32"',
        //     // 'updated_at' => '"2021-06-19 15:38:32"',
        // ]);
        // apiOrden::create([
        //     'orden' => '1343243242323',
        //     'cuenta_contrato' => '13432432423',
        //     'rif_O_Cedula' => '134324324',
        //     'serial' => '1343243232',
        //     'creado' => 'jorgecjja',
        //     'asignado' => 'carcas',
        //     'cola' => 'iiciañ',
        //     'estado' => 'cerrado',
        //     'tipo' => 'servicio',
        //     // 'created_at' => '"2021-06-19 15:38:32"',
        //     // 'updated_at' => '"2021-06-19 15:38:32"',
        // ]);
    }
}
