<?php

use Illuminate\Database\Seeder;
use App\parroquia;

class parroquiaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $parroquia = new parroquia();
        $parroquia->parroquia = 'ALTAGRACIA';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'ANTIMANO';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'CANDELARIA';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'CARICUAO';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'CATEDRAL';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'COCHE';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'EL JUNQUITO';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'EL PARAISO';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'EL RECREO';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'EL VALLE';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'LA PASTORA';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'LA VEGA';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'MACARAO';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'SAN AGUSTIN';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'SAN BERNARDINO';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'SAN JOSE';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'SAN JUAN';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'SAN PEDRO';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'SANTA ROSALIA';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'SANTA TERESA';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'SUCRE';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = '23 DE ENERO';
        $parroquia->municipio_id = 1;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = '23 DE ENERO';
        $parroquia->municipio_id = 1;
        $parroquia->save();

        $parroquia = new parroquia();
        $parroquia->parroquia = 'ARAGUITA';
        $parroquia->municipio_id = 2;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'RIBAS';
        $parroquia->municipio_id = 2;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'Caucagua';
        $parroquia->municipio_id = 2;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'Arévalo González';
        $parroquia->municipio_id = 2;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'Capaya';
        $parroquia->municipio_id = 2;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'El Café';
        $parroquia->municipio_id = 2;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'Marizapa';
        $parroquia->municipio_id = 2;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'Panaquire';
        $parroquia->municipio_id = 2;
        $parroquia->save();

        $parroquia = new parroquia();
        $parroquia->parroquia = 'Ocumare del Tuy';
        $parroquia->municipio_id = 13;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = ' Santa Bárbara';
        $parroquia->municipio_id = 13;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'La Democracia';
        $parroquia->municipio_id = 13;
        $parroquia->save();

        $parroquia = new parroquia();
        $parroquia->parroquia = 'San José de Barlovento';
        $parroquia->municipio_id = 3;
        $parroquia->save();
        $parroquia = new parroquia();
        $parroquia->parroquia = 'Cumbo';
        $parroquia->municipio_id = 3;
        $parroquia->save();


    }
}
