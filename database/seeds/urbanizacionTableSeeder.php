<?php

use Illuminate\Database\Seeder;
use App\urbanizacion;

class urbanizacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // $urbanizacion = new urbanizacion();
        // $urbanizacion->urbanizacion = 'san basilio';
        // $urbanizacion->parroquia_id = 1;
        // $urbanizacion->save();
        // $urbanizacion = new urbanizacion();
        // $urbanizacion->urbanizacion = 'caroni';
        // $urbanizacion->parroquia_id = 2;
        // $urbanizacion->save();
        urbanizacion::create([
            'urbanizacion' => 'San Basilio',
            'parroquia_id' => 32,
        ]);
        urbanizacion::create([
            'urbanizacion' => 'ARAGUITA',
            'parroquia_id' => 32,
        ]);
        urbanizacion::create([
            'urbanizacion' => 'Candelero',
            'parroquia_id' => 32,
        ]);
    }
}
