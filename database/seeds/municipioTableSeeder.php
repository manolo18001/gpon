<?php

use Illuminate\Database\Seeder;
use App\municipio;

class municipioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $municipio = new municipio();
        $municipio->municipio = 'LIBERTADOR';
        $municipio->ciudad_id = 1;
        $municipio->save();

        $municipio = new municipio();
        $municipio->municipio = 'ACEVEDO';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Andrés Bello';
        $municipio->ciudad_id = 4;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Baruta';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Brión';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Buroz';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Carrizal';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Chacao';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Cristóbal Rojas';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = ' El Hatillo';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Guaicaipuro';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Independencia';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Lander';
        $municipio->ciudad_id = 3;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Páez';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Paz Castillo';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = ' Pedro Gual';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Simón Bolívar';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Sucre';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Urdaneta';
        $municipio->ciudad_id = 2;
        $municipio->save();
        $municipio = new municipio();
        $municipio->municipio = 'Zamora';
        $municipio->ciudad_id = 2;
        $municipio->save();
    }
}
