<?php

use App\tipo;
use Illuminate\Database\Seeder;

class tipoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        tipo::created([
            'Tipo'=>'UPLINK'
        ]);
        tipo::created([
            'Tipo'=>'GPON'
        ]);
    }
}
