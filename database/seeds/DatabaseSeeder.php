<?php

use App\apiOrden;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Factory;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(estadoTableSeeder::class);
        $this->call(cuidadTableSeeder::class);
        $this->call(municipioTableSeeder::class);
        $this->call(parroquiaTableSeeder::class);
        $this->call(urbanizacionTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PermisosTableSeeder::class);
        // factory(apiOrden::class())->create();
        $this->call(apiOrdenTableSeeder::class);
        $this->call(statusTableSeeder::class);
        $this->call(ontTipoTableSeeder::class);
        $this->call(ontUsuariosTableSeeder::class);
	    $this->call(tipoTableSeeder::class);

    }
}

