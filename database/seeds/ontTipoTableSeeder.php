<?php

use Illuminate\Database\Seeder;
use App\ont_tipo;

class ontTipoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $ont_tipo = new ont_tipo();
        // $ont_tipo->ID_TIPOS_ONT = 'ALTAGRACIA';
        $ont_tipo->Clasificacion = 'Solo Internet';
        $ont_tipo->save();
        $ont_tipo = new ont_tipo();
        // $ont_tipo->ID_TIPOS_ONT = 'ALTAGRACIA';
        $ont_tipo->Clasificacion = 'Internet + VoIP';
        $ont_tipo->save();
        $ont_tipo = new ont_tipo();
        // $ont_tipo->ID_TIPOS_ONT = 'ALTAGRACIA';
        $ont_tipo->Clasificacion = 'Internet + IPTV';
        $ont_tipo->save();
        $ont_tipo = new ont_tipo();
        // $ont_tipo->ID_TIPOS_ONT = 'ALTAGRACIA';
        $ont_tipo->Clasificacion = 'Internet + VOD';
        $ont_tipo->save();
        $ont_tipo = new ont_tipo();
        // $ont_tipo->ID_TIPOS_ONT = 'ALTAGRACIA';
        $ont_tipo->Clasificacion = 'Internet + VoIP + IPTV';
        $ont_tipo->save();
        $ont_tipo = new ont_tipo();
        // $ont_tipo->ID_TIPOS_ONT = 'ALTAGRACIA';
        $ont_tipo->Clasificacion = 'Internet + VoIP + VOD';
        $ont_tipo->save();







    }
}
