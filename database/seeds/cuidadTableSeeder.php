<?php

use Illuminate\Database\Seeder;
use App\ciudad;

class cuidadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $ciudad = new ciudad();
        $ciudad->ciudad = 'CARACAS';
        $ciudad->estado_id = 1;
        $ciudad->save();

        $ciudad = new ciudad();
        $ciudad->ciudad = 'CAUCAGUA';
        $ciudad->estado_id = 2;
        $ciudad->save();
        $ciudad = new ciudad();
        $ciudad->ciudad = 'OCUMARE DEL TUY';
        $ciudad->estado_id = 2;
        $ciudad->save();
        $ciudad = new ciudad();
        $ciudad->ciudad = 'BARLOVENTO';
        $ciudad->estado_id = 2;
        $ciudad->save();
    }
}
